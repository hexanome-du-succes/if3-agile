package com.insa.h4112.view.table;

import com.insa.h4112.controller.Events;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.CheckpointType;
import com.insa.h4112.model.domain.TimeInterval;
import com.insa.h4112.view.CheckpointSelectionType;
import com.insa.h4112.view.util.Colors;
import com.insa.h4112.view.util.TimeFormatter;

import java.awt.*;
import java.beans.PropertyChangeSupport;
import java.util.Map;

/**
 * View to display a checkpoint (table view)
 *
 * @author Pierre-Yves GENEST
 */
public class CheckpointTableView {

    /**
     * Displayed checkpoint
     */
    private Checkpoint checkpoint;

    /**
     * Color of delivery (linked with other views)
     */
    private Color deliveryColor;

    /**
     * Type of checkpoint
     */
    private CheckpointType type;

    /**
     * Selection type of this checkpoint
     */
    private CheckpointSelectionType selection;

    /**
     * To fire events
     */
    private PropertyChangeSupport propertyChangeSupport;

    /**
     * Converts a CheckpointType instance to its String representation.
     */
    private final static Map<CheckpointType, String> checkpointTypeToString = Map.ofEntries(
            Map.entry(CheckpointType.WAREHOUSE, "Entrepôt"),
            Map.entry(CheckpointType.PICK_UP, "Récupération"),
            Map.entry(CheckpointType.DELIVER, "Livraison")
    );

    /**
     * Constructor
     *
     * @param checkpoint checkpoint to display
     * @param type       type of checkpoint (ie warehouse, pickup, deliver)
     * @param color      color of checkpoint
     */
    public CheckpointTableView(Checkpoint checkpoint, CheckpointType type, Color color) {
        this.checkpoint = checkpoint;
        this.type = type;
        deliveryColor = color;
        selection = CheckpointSelectionType.NONE;
        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    /**
     * Set selection type
     *
     * @param selection the selection type
     */
    public void setSelected(CheckpointSelectionType selection) {
        this.selection = selection;
    }

    /**
     * Return value at column index
     *
     * @param column column index
     * @return value at column index
     */
    public Object getValueAt(int column) {
        // Create text of cell
        String text = "";
        switch (column) {
            case 1: // Type of checkpoint
                text = checkpointTypeToString.get(type);
                break;
            case 2: // Passage time at checkpoint
                if (checkpoint.getPassageTime() != null) {
                    text = TimeFormatter.getShort(checkpoint.getPassageTime());
                }
                break;
            case 3: // Time spent at checkpoint
                if (type != CheckpointType.WAREHOUSE) { // Display only if we are not at the warehouse
                    text = TimeFormatter.getLiteral(checkpoint.getDuration());
                }
                break;
            case 4: // Time intervall to go to checkpoint
                if (type != CheckpointType.WAREHOUSE) {
                    TimeInterval interval = checkpoint.getInterval();
                    text = TimeFormatter.getShort(interval.getStartTime()) + " - " + TimeFormatter.getShort(interval.getEndTime());
                }
                break;
            default:
                text = "";
        }

        // background color of cell
        Color background;
        if (column == 0) {    // First column = color of delivery
            background = deliveryColor;
        } else {
            switch (selection) {
                case SELECTED:
                    background = Colors.CHECKPOINT_SELECTED_COLOR;
                    break;
                case ASSOCIATED:
                    background = Colors.CHECKPOINT_ASSOCIATED_SELECTED_COLOR;
                    break;
                default:
                    background = Color.WHITE;
            }
        }

        // Text color
        Color textColor = null;
        if (type != CheckpointType.WAREHOUSE && checkpoint.getInterval() != null && checkpoint.getPassageTime() != null && !checkpoint.getInterval().contains(checkpoint.getPassageTime())) {
            textColor = Color.RED;
        }

        return CellContent.cellContent(text, background, textColor);
    }

    /**
     * Return displayed checkpoint
     *
     * @return displayed checkpoint
     */
    public Checkpoint getCheckpoint() {
        return checkpoint;
    }

    /**
     * Click on this checkpoint
     */
    public void click() {
        propertyChangeSupport.firePropertyChange(Events.SELECT_CHECKPOINT_EVENT, null, checkpoint);
    }

    /**
     * Getter
     *
     * @return property change support
     */
    public PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }
}
