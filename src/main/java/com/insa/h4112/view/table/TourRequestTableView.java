package com.insa.h4112.view.table;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.model.domain.CheckpointType;
import com.insa.h4112.model.domain.Delivery;
import com.insa.h4112.model.domain.TourRequest;
import com.insa.h4112.view.util.Colors;

/**
 * View that represents a tour request
 *
 * @author Pierre-Yves GENEST
 */
public class TourRequestTableView extends ModelTableView {

    /**
     * Displayed tour request
     */
    private TourRequest tourRequest;

    /**
     * Factory
     */
    private Controller controller;

    /**
     * Constructor
     */
    public TourRequestTableView(Controller controller) {
        super(controller);
        tourRequest = null;
        this.controller = controller;
    }

    /**
     * set tour request
     *
     * @param tourRequest tour request
     */
    public void setTourRequest(TourRequest tourRequest) {
        this.tourRequest = tourRequest;
        checkpointTableViews.clear();

        if (tourRequest != null) {
            checkpointTableViews.add(checkpointManager.getOrCreate(tourRequest.getWarehouse().getStart(), CheckpointType.WAREHOUSE, Colors.generateColor(tourRequest.getWarehouse().getSeed())));

            for (Delivery delivery : tourRequest.getDeliveries()) {
                checkpointTableViews.add(checkpointManager.getOrCreate(delivery.getPickUp(), CheckpointType.PICK_UP, Colors.generateColor(delivery.getSeed())));
                checkpointTableViews.add(checkpointManager.getOrCreate(delivery.getDeliver(), CheckpointType.DELIVER, Colors.generateColor(delivery.getSeed())));
            }

            checkpointTableViews.add(checkpointManager.getOrCreate(tourRequest.getWarehouse().getEnd(), CheckpointType.WAREHOUSE, Colors.generateColor(tourRequest.getWarehouse().getSeed())));
        }

        this.fireTableDataChanged();
    }
}
