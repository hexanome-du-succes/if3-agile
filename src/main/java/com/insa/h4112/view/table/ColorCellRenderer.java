package com.insa.h4112.view.table;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * JTable cell colored with a background color
 *
 * @author Pierre-Yves GENEST
 */
public class ColorCellRenderer extends DefaultTableCellRenderer {

    /**
     * Render table cell
     *
     * @param table      table to render
     * @param value      value inside cell : MUST be a color
     * @param isSelected if cell is selected
     * @param hasFocus   if celle has focus
     * @param row        row of cell
     * @param col        column of cell
     * @return cell component
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
        //Cells are by default rendered as a JLabel.
        JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
        cell.setHorizontalAlignment(SwingConstants.CENTER);

        CellContent content = (CellContent) value;
        cell.setBackground(content.getCellColor());    // Setting background color
        cell.setText(content.getCellText());           // Setting text

        if (content.getTextColor() != null) {
            // Setting specified in cellcontent
            cell.setForeground(content.getTextColor());
        } else if (luminance(content.getCellColor()) < 0.5) {
            // If color is too dark, change text color to white
            cell.setForeground(Color.WHITE);
        } else {
            cell.setForeground(Color.BLACK);
        }
        return cell;
    }

    /**
     * Compute luminance of a color
     *
     * @param color color
     * @return luminance
     */
    private double luminance(Color color) {
        // see : https://stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color
        return (0.2126 * color.getRed() + 0.7152 * color.getGreen() + 0.0722 * color.getBlue()) / 255.;
    }
}
