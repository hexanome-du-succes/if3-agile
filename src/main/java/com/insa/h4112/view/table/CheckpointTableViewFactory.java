package com.insa.h4112.view.table;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.CheckpointType;

import java.awt.*;
import java.util.HashMap;

/**
 * Factory to interact checkpoint views (table view)
 *
 * @author Pierre-Yves GENEST
 */
public class CheckpointTableViewFactory {

    /**
     * Application controller
     */
    private Controller controller;

    /**
     * All created checkpoints views
     */
    private HashMap<Checkpoint, CheckpointTableView> checkpoints;

    /**
     * Constructor
     *
     * @param controller Application controller
     */
    public CheckpointTableViewFactory(Controller controller) {
        this.controller = controller;
        checkpoints = new HashMap<>();
    }

    /**
     * Get or create a checkpoint view
     *
     * @param checkpoint checkpoint
     * @param type       type of checkpoint
     * @param color      checkpoint color
     * @return checkpoint view
     */
    public CheckpointTableView getOrCreate(Checkpoint checkpoint, CheckpointType type, Color color) {
        CheckpointTableView checkpointView = checkpoints.get(checkpoint);

        if (checkpointView == null) {
            checkpointView = new CheckpointTableView(checkpoint, type, color);
            checkpointView.getPropertyChangeSupport().addPropertyChangeListener(controller);
            checkpoints.put(checkpoint, checkpointView);
        }

        return checkpointView;
    }

    /**
     * To find a checkpoint view
     *
     * @param checkpoint checkpoint associated to view
     * @return view
     */
    public CheckpointTableView find(Checkpoint checkpoint) {
        return checkpoints.get(checkpoint);
    }
}
