package com.insa.h4112.view.table;

import com.insa.h4112.controller.Controller;

import javax.swing.*;
import java.awt.*;

/**
 * View class containing all the buttons for user interactions with checkpoints (add and delete)
 *
 * @author Paul Goux
 * @author Thomas Zhou
 */
public class ButtonBarTableView extends JPanel {

    /**
     * button to add new checkpoint
     */
    private JButton addCheckpointButton;

    /**
     * button to delete selected checkpoint
     */
    private JButton deleteCheckpointButton;

    /**
     * The controller to manage global application
     */
    private Controller controller = null;

    public ButtonBarTableView() {
        super();
        setLayout(new FlowLayout(FlowLayout.TRAILING, 1, 2));
        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 20));
        addCheckpointButton = new JButton();
        addCheckpointButton.setText("Ajouter une livraison");
        addCheckpointButton.setEnabled(false);
        add(addCheckpointButton);
        deleteCheckpointButton = new JButton();
        deleteCheckpointButton.setText("Supprimer la livraison");
        deleteCheckpointButton.setEnabled(false);
        add(deleteCheckpointButton);
    }

    private void initButtonsActions() {
        deleteCheckpointButton.addActionListener(e -> {
            controller.deleteSelectedCheckpointEvent();
        });
        addCheckpointButton.addActionListener(e -> {
            controller.addDeliveryEvent();
        });
    }

    /**
     * Change the state of a button. A button is either enabled or disabled.
     *
     * @param enable If true the button is enabled. If false the button is disabled.
     */
    public void setDeleteCheckpointButtonEnable(boolean enable) {
        deleteCheckpointButton.setEnabled(enable);
    }

    public void setAddCheckpointButtonEnable(boolean enable) {
        addCheckpointButton.setEnabled(enable);
    }

    /**
     * Setter
     *
     * @param controller controller to use
     */
    public void setController(Controller controller) {
        this.controller = controller;
        if (controller != null) {
            initButtonsActions();
        }
    }
}
