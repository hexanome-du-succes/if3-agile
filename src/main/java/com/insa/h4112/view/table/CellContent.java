package com.insa.h4112.view.table;

import java.awt.*;

/**
 * Cell content (of a jtable)
 * SINGLETON
 *
 * @author Pierre-Yves GENEST
 */
public class CellContent {

    /**
     * Instance
     */
    private static final CellContent instance = new CellContent();

    /**
     * Background color of cell
     */
    private Color cellColor;

    /**
     * Text color (default null)
     */
    private Color textColor;

    /**
     * Text inside cell
     */
    private String cellText;


    /**
     * Deleted constructor
     */
    private CellContent() {
    }

    /**
     * Get instance of cell
     *
     * @param cellText  text of cell
     * @param cellColor color of cell
     *                  <<<<<<< HEAD
     * @param textColor text color
     * @return cellcontent reference
     * >>>>>>> master
     */
    public static CellContent cellContent(String cellText, Color cellColor, Color textColor) {
        instance.cellColor = cellColor;
        instance.cellText = cellText;
        instance.textColor = textColor;
        return instance;
    }

    /**
     * Get cell color
     *
     * @return cell color
     */
    public Color getCellColor() {
        return cellColor;
    }

    /**
     * Get cell text
     *
     * @return cell text
     */
    public String getCellText() {
        return cellText;
    }

    /**
     * Getter
     *
     * @return text color
     */
    public Color getTextColor() {
        return textColor;
    }

    /**
     * Describe this object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "CellContent{" +
                "cellColor=" + cellColor +
                ", textColor=" + textColor +
                ", cellText='" + cellText + '\'' +
                '}';
    }
}
