package com.insa.h4112.view.cartographic;

import com.insa.h4112.model.domain.Path;
import com.insa.h4112.model.domain.Tour;

import java.awt.*;
import java.util.HashMap;

/**
 * View that display tour on map (cartographic view)
 *
 * @author Pierre-Yves GENEST
 */
public class TourCartographicView implements View {

    /**
     * Tour shown by this view
     */
    private Tour tour;

    /**
     * Link between paths and pathviews
     */
    private HashMap<Path, PathCartographicView> pathPathSchemeViewHashMap;

    /**
     * Constructor
     */
    public TourCartographicView() {
        tour = null;
        pathPathSchemeViewHashMap = new HashMap<>();
    }

    /**
     * Change tour shown by this view
     *
     * @param tour new tour
     * @param map  mapview (to get sections)
     */
    public void setTour(Tour tour, MapCartographicView map) {
        this.tour = tour;
        pathPathSchemeViewHashMap.clear();

        if (tour != null) {
            for (Path path : tour.getAllPaths()) {
                pathPathSchemeViewHashMap.put(path, new PathCartographicView(path, map));
            }
        }
    }

    /**
     * To pain this view
     *
     * @param graphics2D graphics element
     * @param unit
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        for (PathCartographicView pathCartographicView : pathPathSchemeViewHashMap.values()) {
            pathCartographicView.paint(graphics2D, unit);
        }
    }
}
