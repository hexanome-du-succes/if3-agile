package com.insa.h4112.view.cartographic.specific_views;

import com.insa.h4112.view.cartographic.IntersectionCartographicView;
import com.insa.h4112.view.util.ShapeViewFactory;

import java.awt.*;

/**
 * View to select an intersection
 *
 * @author Thomas Zhou
 */
public class SelectedIntersectionView implements SpecificView {
    /**
     * Selected checkpoint
     */
    private IntersectionCartographicView selectedIntersection;

    /**
     * Shape associated to selected intersection
     */
    private Shape selectedShape;

    /**
     * Color to paint selectedShape
     */
    private static final Color SELECTED_COLOR = Color.GREEN;

    /**
     * Constructor
     *
     * @param selectedIntersection selected checkpoint
     */
    public SelectedIntersectionView(IntersectionCartographicView selectedIntersection) {
        this.selectedIntersection = selectedIntersection;

        Rectangle bounds = selectedIntersection.getBounds();
        selectedShape = ShapeViewFactory.createIntersectionShape(bounds.x, bounds.y, bounds.width, bounds.height);
    }

    /**
     * To paint a component on screen
     *
     * @param graphics2D graphics element
     * @param unit       reference unit
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        Rectangle bounds = selectedIntersection.getBounds();
        ShapeViewFactory.updateIntersectionShape(selectedShape, bounds.x - unit, bounds.y - unit, bounds.width + 2 * unit, bounds.height + 2 * unit);

        graphics2D.setColor(SELECTED_COLOR);
        graphics2D.fill(selectedShape);
    }
}
