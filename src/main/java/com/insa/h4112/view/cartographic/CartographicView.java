package com.insa.h4112.view.cartographic;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.Events;
import com.insa.h4112.model.domain.*;
import com.insa.h4112.view.ModelView;
import com.insa.h4112.view.cartographic.specific_views.AssociatedSelectedCheckpointView;
import com.insa.h4112.view.cartographic.specific_views.SelectedCheckpointView;
import com.insa.h4112.view.cartographic.specific_views.SelectedIntersectionView;
import com.insa.h4112.view.cartographic.specific_views.SpecificView;
import com.insa.h4112.view.util.Colors;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;

/**
 * Main container of this view
 * Represented by a map
 *
 * @author Pierre-Yves GENEST
 */
public class CartographicView extends JPanel implements ModelView, PropertyChangeListener {
    /**
     * Map of application (sections and intersections)
     */
    private MapCartographicView mapView;

    /**
     * Current tourRequest (essentially checkpoints)
     */
    private TourRequestCartographicView tourRequestView;

    /**
     * Current tour (paths)
     */
    private TourCartographicView tourView;

    /**
     * Application controller
     */
    private Controller controller = null;

    ////////// Model objects /////////
    /**
     * Model map
     */
    private Map map;

    ////////// To manage move/zoom inside view //////////
    /**
     * Current translation in cartographic view
     */
    private Point.Double translation;

    /**
     * Scale factor of cartographic view (including zoom)
     */
    private double scale;

    /**
     * Zoom
     */
    private double zoom;

    /**
     * Last position (mouse drag)
     */
    private Point lastPoint;


    ////////// Specific views //////////
    /**
     * A list of view to paint (after "normal" views)
     */
    private java.util.Map<Object, SpecificView> specificViews;

    /**
     * To highlight specific views
     */
    private boolean areSpecificViewsHighlighted;

    /**
     * Constructor
     */
    public CartographicView() {
        // Model parameters
        map = null;

        // Initialize parameters
        mapView = new MapCartographicView();
        tourRequestView = new TourRequestCartographicView();
        tourView = new TourCartographicView();
        specificViews = new HashMap<>();
        areSpecificViewsHighlighted = false;

        translation = new Point.Double();
        scale = 1;
        zoom = 1;
        lastPoint = null;

        // Handle zoom
        addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                Point.Double beforeZoom = new Point.Double(e.getX() / scale, e.getY() / scale); // Convert to xy in map

                zoom = Math.max(1., zoom * (e.getPreciseWheelRotation() > 0 ? 0.85 : 1.15));
                computeScale();

                Point.Double afterZoom = new Point.Double(e.getX() / scale, e.getY() / scale);  // Convert to xy in map

                // Translate to center zoom to mouse position
                translation.x += beforeZoom.x - afterZoom.x;
                translation.y += beforeZoom.y - afterZoom.y;
                repaint();
            }
        });

        // Handle move
        addMouseListener(new MouseAdapter() {
            /**
             * {@inheritDoc}$
             *
             * @param e
             */
            @Override
            public void mouseReleased(MouseEvent e) {
                lastPoint = null;
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            /**
             * Invoked when a mouse button is pressed on a component and then
             * dragged.  Mouse drag events will continue to be delivered to
             * the component where the first originated until the mouse button is
             * released (regardless of whether the mouse position is within the
             * bounds of the component).
             *
             * @param e
             */
            @Override
            public void mouseDragged(MouseEvent e) {
                Point newPoint = e.getPoint();

                if (lastPoint != null) {
                    translation.x += (-newPoint.x + lastPoint.x) / scale;
                    translation.y += (-newPoint.y + lastPoint.y) / scale;

                    repaint();
                }

                lastPoint = newPoint;
            }
        });

        // Handle click
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Point.Double coordinates = viewToXy(e.getPoint());
                mapView.click(coordinates);
                tourRequestView.click(coordinates);
            }
        });

        this.setBackground(Color.WHITE);
        this.setVisible(true);
    }

    /**
     * Set current map
     *
     * @param map map to set
     */
    @Override
    public void setMap(Map map) {
        if (this.map != map) {
            this.map = map;

            zoom = 1;
            scale = 1;
            translation = new Point.Double();
            lastPoint = null;

            mapView.setMap(map);
            this.repaint();
        }
    }

    /**
     * Set current tour request
     *
     * @param tourRequest tour request to set
     */
    @Override
    public void setTourRequest(TourRequest tourRequest) {
        tourRequestView.setTourRequest(tourRequest, mapView, controller);
        this.repaint();
    }

    /**
     * Set current tour
     *
     * @param tour tour to set
     */
    @Override
    public void setTour(Tour tour) {
        tourView.setTour(tour, mapView);
        this.repaint();
    }

    /**
     * To paint map on screen
     *
     * @param g graphic object
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D graphics2D = (Graphics2D) g;

        // Set anti-aliasing
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON
        );
        graphics2D.setRenderingHints(rh);

        // Compute new scale
        computeScale();

        // Apply scale and translation
        graphics2D.scale(scale, scale);
        graphics2D.translate(-mapView.getBounds().x - translation.x, -mapView.getBounds().y - translation.y);
        double referenceUnit = Math.max(3 / scale, 4.);

        mapView.paint(graphics2D, referenceUnit);

        tourView.paint(graphics2D, referenceUnit);
        tourRequestView.paint(graphics2D, referenceUnit);

        // fade views out to highlight specific views if needed
        if (areSpecificViewsHighlighted) {
            Rectangle bounds = mapView.getBounds();

            graphics2D.setColor(Colors.FADED_MAPVIEW_COLOR);
            graphics2D.fillRect(bounds.x - (int) (10 * referenceUnit), bounds.y - (int) (10 * referenceUnit), bounds.width + (int) (20 * referenceUnit), bounds.height + (int) (20 * referenceUnit));
        }

        // Paint all specific views
        for (View specificView : specificViews.values()) {
            specificView.paint(graphics2D, referenceUnit);
        }
    }

    /**
     * Compute scale
     */
    private void computeScale() {
        // Ensure map ratio
        double scaleX = getWidth() / (double) (mapView.getBounds().width + 1);
        double scaleY = getHeight() / (double) (mapView.getBounds().height + 1);

        // Keep minimum scale (to ensure that map fit on screen)
        scale = Math.min(scaleX, scaleY) * zoom;
    }

    /**
     * Convert view xy (on screen) to real xy (map)
     *
     * @param screenCoordinates screen coordinates
     * @return map coordinates
     */
    private Point.Double viewToXy(Point screenCoordinates) {
        Point.Double xy = new Point.Double(screenCoordinates.x / scale, screenCoordinates.y / scale);   // Apply scale

        // Add translation (and bounds)
        xy.x += translation.x + mapView.getBounds().x;
        xy.y += translation.y + mapView.getBounds().y;

        return xy;
    }

    /**
     * To set the controller
     *
     * @param controller Application controller
     */
    public void setController(Controller controller) {
        this.controller = controller;
        mapView.setController(controller);
    }

    /**
     * Receive property change event
     *
     * @param propertyChangeEvent property change event
     */
    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        switch (propertyChangeEvent.getPropertyName()) {
            //That's normal, it's like an OR
            case Events.ADD_DELIVERY_FROM_TOUR_EVENT:
            case Events.REORDER_CHECKPOINT_EVENT:
            case Events.REMOVE_DELIVERY_FROM_TOUR_EVENT:
                setTour((Tour) propertyChangeEvent.getNewValue());
                break;
            case Events.ADD_DELIVERY_FROM_TOUR_REQUEST_EVENT:
            case Events.REMOVE_DELIVERY_FROM_TOUR_REQUEST_EVENT:
                setTourRequest((TourRequest) propertyChangeEvent.getNewValue());
                break;
            default:
                break;
        }
    }

    /**
     * To get bounds around mapview
     *
     * @return bounds of mapview
     */
    public Rectangle getMapViewBounds() {
        return mapView.getBounds();
    }

    /**
     * Select a checkpoint
     *
     * @param selectedCheckpoint           selected checkpoint
     * @param associatedSelectedCheckpoint associated selected checkpoint
     */
    public void selectCheckpoints(Checkpoint selectedCheckpoint, Checkpoint associatedSelectedCheckpoint) {
        // Search selected view, and create specific view
        CheckpointCartographicView selectedCheckpointView = tourRequestView.getCheckpointManager().find(selectedCheckpoint);
        if (selectedCheckpointView != null) {    // selected view can be null (in case of warehouse)
            SelectedCheckpointView selectedCheckpointSpecificView = new SelectedCheckpointView(selectedCheckpointView);
            addSpecificView(selectedCheckpoint, selectedCheckpointSpecificView);
        }

        // Search associated selected view, and create specific view
        CheckpointCartographicView associatedSelectedCheckpointView = tourRequestView.getCheckpointManager().find(associatedSelectedCheckpoint);
        if (associatedSelectedCheckpointView != null) {    // associated view can be null (in case of warehouse)
            AssociatedSelectedCheckpointView associatedSelectedCheckpointSpecificView = new AssociatedSelectedCheckpointView(associatedSelectedCheckpointView);
            addSpecificView(associatedSelectedCheckpoint, associatedSelectedCheckpointSpecificView);
        }
    }

    /**
     * Unselect checkpoint
     *
     * @param selectedCheckpoint           selected checkpoint
     * @param associatedSelectedCheckpoint associated selected checkpoint
     */
    public void unselectCheckpoints(Checkpoint selectedCheckpoint, Checkpoint associatedSelectedCheckpoint) {
        // Remove views
        removeSpecificView(selectedCheckpoint);
        removeSpecificView(associatedSelectedCheckpoint);
    }

    /**
     * Select an intersection on the map
     *
     * @param intersection intersection to select
     */
    public void selectIntersection(Intersection intersection) {
        SpecificView specificView = new SelectedIntersectionView(mapView.findIntersectionView(intersection));
        addSpecificView(intersection, specificView);
    }

    /**
     * Select an intersection on the map
     *
     * @param intersection intersection to select
     */
    public void unselectIntersection(Intersection intersection) {
        removeSpecificView(intersection);
    }

    /**
     * Highlight a single checkpoint
     *
     * @param checkpoint checkpoint
     */
    public void highlightCheckpoint(Checkpoint checkpoint) {
        CheckpointCartographicView selectedCheckpointView = tourRequestView.getCheckpointManager().find(checkpoint);
        if (selectedCheckpointView != null) {    // selected view can be null (in case of warehouse)
            AssociatedSelectedCheckpointView highlightedCheckpointSpecificView = new AssociatedSelectedCheckpointView(selectedCheckpointView);
            addSpecificView(checkpoint, highlightedCheckpointSpecificView);
        }
    }

    /**
     * Unhighlight a single checkpoint
     *
     * @param checkpoint checkpoint
     */
    public void unHighlightCheckpoint(Checkpoint checkpoint) {
        removeSpecificView(checkpoint);
    }

    /**
     * Add a specific view
     *
     * @param key  key to identify specific view
     * @param view view to add
     */
    private void addSpecificView(Object key, SpecificView view) {
        specificViews.put(key, view);
        view.activate(this);
        repaint();
    }

    /**
     * Remove a specific view
     *
     * @param key key that identify specific view
     */
    private void removeSpecificView(Object key) {
        SpecificView specificView = specificViews.get(key);

        if (specificView != null) {
            specificView.deactivate(this);
            specificViews.remove(key);
        }
        repaint();
    }

    /**
     * To highlight specific views
     *
     * @param areSpecificViewsHighlighted if specific view are highlighted
     */
    public void setAreSpecificViewsHighlighted(boolean areSpecificViewsHighlighted) {
        this.areSpecificViewsHighlighted = areSpecificViewsHighlighted;
        repaint();
    }
}
