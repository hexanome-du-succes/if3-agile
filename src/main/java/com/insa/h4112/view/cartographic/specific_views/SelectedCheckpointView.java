package com.insa.h4112.view.cartographic.specific_views;

import com.insa.h4112.view.cartographic.CartographicView;
import com.insa.h4112.view.cartographic.CheckpointCartographicView;
import com.insa.h4112.view.util.Colors;

import java.util.Timer;
import java.util.TimerTask;

/**
 * View to select a checkpoint
 *
 * @author Pierre-Yves GENEST
 */
public class SelectedCheckpointView extends HighlightedCheckpointView {

    /**
     * To call asynchronously a method
     */
    private static final Timer TIMER = new Timer();

    /**
     * Delay before finishing selected checkpoint animation
     */
    private static final int REPAINT_DELAY = 1000;

    /**
     * Constructor
     *
     * @param selectedCheckpoint selected checkpoint
     */
    public SelectedCheckpointView(CheckpointCartographicView selectedCheckpoint) {
        super(selectedCheckpoint, Colors.CHECKPOINT_SELECTED_COLOR);
    }

    /**
     * Deactivate all effects on cartographic view
     *
     * @param view view
     */
    @Override
    public void deactivate(CartographicView view) {
        view.setAreSpecificViewsHighlighted(false);
    }

    /**
     * Activate all effects on cartographic view
     *
     * @param view view
     */
    @Override
    public void activate(CartographicView view) {
        view.setAreSpecificViewsHighlighted(true);

        TIMER.schedule(new TimerTask() {
            @Override
            public void run() {
                view.setAreSpecificViewsHighlighted(false);
            }
        }, REPAINT_DELAY);
    }
}
