package com.insa.h4112.view.cartographic;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.CheckpointType;

import java.awt.*;
import java.util.HashMap;

/**
 * Factory to interact with checkpoint cartographic view
 *
 * @author Pierre-Yves GENEST
 */
public class CheckpointCartographicViewFactory {

    /**
     * Application controller
     */
    private Controller controller;

    /**
     * All checkpoints views created
     */
    private HashMap<Checkpoint, CheckpointCartographicView> checkpoints;

    /**
     * Constructor
     *
     * @param controller Controller
     */
    public CheckpointCartographicViewFactory(Controller controller) {
        checkpoints = new HashMap<>();

        this.controller = controller;
    }

    /**
     * Get or create a checkpoint view
     *
     * @param checkpoint checkpoint
     * @param type       type of checkpoint
     * @param map        map that contains all intersections
     * @param color      checkpoint color (delivery/warehouse color)
     * @return checkpoint view
     */
    public CheckpointCartographicView getOrCreate(Checkpoint checkpoint, CheckpointType type, Color color, MapCartographicView map) {
        CheckpointCartographicView checkpointView = checkpoints.get(checkpoint);

        if (checkpointView == null) {
            checkpointView = new CheckpointCartographicView(checkpoint, type, color, map);
            checkpointView.addPropertyChangeListener(controller);
            checkpoints.put(checkpoint, checkpointView);
        }

        return checkpointView;
    }

    /**
     * To find a checkpoint view
     *
     * @param checkpoint checkpoint associated to view
     * @return view
     */
    public CheckpointCartographicView find(Checkpoint checkpoint) {
        return checkpoints.get(checkpoint);
    }

    /**
     * Select a checkpoint depending on its coordinates
     *
     * @param coordinates coordinates to select
     */
    public void select(Point.Double coordinates) {
        checkpoints.forEach((checkpoint, view) -> view.click(coordinates));
    }
}
