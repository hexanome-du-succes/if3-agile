package com.insa.h4112.view.cartographic;

import com.insa.h4112.model.domain.CheckpointType;
import com.insa.h4112.model.domain.Delivery;
import com.insa.h4112.view.util.Colors;

import java.awt.*;

/**
 * View representing a delivery (cartographic view)
 *
 * @author Pierre-Yves GENEST
 */
public class DeliveryCartographicView implements View {

    /**
     * Delivery displayed by this view
     */
    private Delivery delivery;

    /**
     * Pick up checkpoint displayed
     */
    private CheckpointCartographicView pickUpCheckpointCartographicView;

    /**
     * Deliver checkpoint displayed
     */
    private CheckpointCartographicView deliverCheckpointCartographicView;

    /**
     * Color associated to this delivery
     */
    private Color deliveryColor;

    /**
     * Constructor
     *
     * @param delivery delivery to display
     * @param map      map object (to get intersection)
     * @param manager  manager to create checkpoint
     */
    public DeliveryCartographicView(Delivery delivery, MapCartographicView map, CheckpointCartographicViewFactory manager) {
        this.delivery = delivery;

        // Compute delivery color
        deliveryColor = Colors.generateColor(delivery.getSeed());

        // Create checkpoint views
        pickUpCheckpointCartographicView = manager.getOrCreate(delivery.getPickUp(), CheckpointType.PICK_UP, deliveryColor, map);
        deliverCheckpointCartographicView = manager.getOrCreate(delivery.getDeliver(), CheckpointType.DELIVER, deliveryColor, map);

    }

    /**
     * To display delivery
     *
     * @param graphics2D graphics element
     * @param unit
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        // Paint checkpoints
        pickUpCheckpointCartographicView.paint(graphics2D, unit);
        deliverCheckpointCartographicView.paint(graphics2D, unit);
    }
}
