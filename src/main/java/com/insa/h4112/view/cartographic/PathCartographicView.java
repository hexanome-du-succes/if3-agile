package com.insa.h4112.view.cartographic;

import com.insa.h4112.model.domain.Path;
import com.insa.h4112.view.util.Colors;

import java.awt.*;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * To display a path on cartographic view
 *
 * @author Pierre-Yves GENEST
 */
public class PathCartographicView implements View {

    /**
     * Path displayed by this view
     */
    private Path path;

    /**
     * Sectionviews of this path
     */
    private Collection<SectionCartographicView> sectionCartographicViews;

    /**
     * Section stroke
     */
    private BasicStroke stroke = new BasicStroke(10);

    /**
     * Factor to paint path
     */
    private static final double PATH_FACTOR = 2.;

    /**
     * Section color
     */
    private Color color;

    /**
     * Arrow frequency
     * We draw an arrow to indicate direction after X sections
     */
    private static final int arrowFrequency = 5;

    /**
     * Constructor
     *
     * @param path Path to display
     * @param map  Mapview (to get section views)
     */
    public PathCartographicView(Path path, MapCartographicView map) {
        this.path = path;

        // Get all sections views
        sectionCartographicViews = path.getSteps().stream().map(map::findSectionView).collect(Collectors.toList());

        // Generate color
        color = Colors.generateRed(path.hashCode());
    }

    /**
     * Display path on screen
     *
     * @param graphics2D graphics element
     * @param unit
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        int i = 0;
        stroke = new BasicStroke((int) (PATH_FACTOR * unit));
        for (SectionCartographicView sectionCartographicView : sectionCartographicViews) {
            sectionCartographicView.paint(graphics2D, color, stroke, (i % arrowFrequency) == 0, unit);
            i++;
        }
    }
}
