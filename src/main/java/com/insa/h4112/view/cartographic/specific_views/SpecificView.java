package com.insa.h4112.view.cartographic.specific_views;

import com.insa.h4112.view.cartographic.CartographicView;
import com.insa.h4112.view.cartographic.View;

/**
 * Specific view to paint on cartographic view
 *
 * @author Pierre-Yves GENEST
 */
public interface SpecificView extends View {

    /**
     * Deactivate all effects on cartographic view
     */
    default void deactivate(CartographicView view) {
    }

    /**
     * Activate all effects on cartographic view
     */
    default void activate(CartographicView view) {
    }
}
