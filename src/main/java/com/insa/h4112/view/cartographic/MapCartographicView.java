package com.insa.h4112.view.cartographic;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Section;

import java.awt.*;
import java.util.HashMap;

/**
 * View class responsible of displaying the map
 *
 * @author Pierre-Yves GENEST
 * @author Paul Goux
 * @author Thomas Zhou
 */
public class MapCartographicView implements View {

    /**
     * All {@link Section} to draw
     */
    private java.util.Map<Section, SectionCartographicView> sectionViewMap;

    /**
     * All {@link Intersection} to draw
     */
    private java.util.Map<Intersection, IntersectionCartographicView> intersectionViewMap;

    /**
     * the map to display
     */
    private Map map;

    /**
     * Controller to use
     */
    private Controller controller;

    ///////////////// Parameters to draw map /////////////////
    /**
     * Bounds around map (minX, minY, width, height)
     */
    private Rectangle bounds;

    /**
     * Constructor
     */
    public MapCartographicView() {
        sectionViewMap = new HashMap<>();
        intersectionViewMap = new HashMap<>();
        bounds = new Rectangle(Integer.MAX_VALUE, Integer.MAX_VALUE, 0, 0);
        this.controller = null;
    }

    /**
     * Update the displayed map of the application
     *
     * @param map new map to draw
     */
    public void setMap(Map map) {
        this.map = map;
        sectionViewMap = new HashMap<>();
        intersectionViewMap = new HashMap<>();

        // Reset precedent parameters
        bounds = new Rectangle(Integer.MAX_VALUE, Integer.MAX_VALUE, 0, 0);

        if (map != null) {
            // Create all sections and intersections
            for (Intersection intersection : map.getAllIntersectionsAsList()) {
                IntersectionCartographicView intersectionCartographicView = new IntersectionCartographicView(intersection);
                intersectionViewMap.put(intersection, intersectionCartographicView);

                // Computing bounds
                Rectangle intersectionBounds = intersectionCartographicView.getBounds();
                if (bounds.x > intersectionBounds.x) {
                    bounds.x = intersectionBounds.x;
                }
                if (bounds.y > intersectionBounds.y) {
                    bounds.y = intersectionBounds.y;
                }
                if (bounds.x + bounds.width < intersectionBounds.x + intersectionBounds.width) {
                    bounds.width = intersectionBounds.x + intersectionBounds.width - bounds.x;
                }
                if (bounds.y + bounds.height < intersectionBounds.y + intersectionBounds.height) {
                    bounds.height = intersectionBounds.y + intersectionBounds.height - bounds.y;
                }

                for (Section section : intersection.getOutgoingSections()) {
                    sectionViewMap.put(section, new SectionCartographicView(section));
                }

                intersectionCartographicView.addPropertyChangeListener(controller);
            }
        }
    }

    /**
     * To paint map on screen
     *
     * @param graphics2D graphic object
     * @param unit
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        // Draw all intersections and sections
        for (IntersectionCartographicView intersectionCartographicView : intersectionViewMap.values()) {
            intersectionCartographicView.paint(graphics2D, unit);
        }
        for (SectionCartographicView sectionCartographicView : sectionViewMap.values()) {
            sectionCartographicView.paint(graphics2D, unit);
        }
    }

    /**
     * To get bounds around map
     *
     * @return bounds
     */
    public Rectangle getBounds() {
        return bounds;
    }

    /**
     * To find a sectionview by its section
     *
     * @param section section associated to sectionview
     * @return sectionView (or null)
     */
    public SectionCartographicView findSectionView(Section section) {
        return sectionViewMap.get(section);
    }

    /**
     * To find an intersectionView by its intersection
     *
     * @param intersection intersection associated to intersectionview
     * @return intersectionView (or null)
     */
    public IntersectionCartographicView findIntersectionView(Intersection intersection) {
        return intersectionViewMap.get(intersection);
    }

    /**
     * Click on coordinates
     *
     * @param coordinates click coordinates
     */
    void click(Point.Double coordinates) {
        IntersectionCartographicView nearestIntersectionCartographicView = null;
        double minDistance = Double.MAX_VALUE;
        for (IntersectionCartographicView intersectionCartographicView : intersectionViewMap.values()) {
            double distance = coordinates.distance(intersectionCartographicView.getCenter());
            if (distance < minDistance) {
                minDistance = distance;
                nearestIntersectionCartographicView = intersectionCartographicView;
            }
        }
        if (nearestIntersectionCartographicView == null)
            return;
        nearestIntersectionCartographicView.click();
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
}
