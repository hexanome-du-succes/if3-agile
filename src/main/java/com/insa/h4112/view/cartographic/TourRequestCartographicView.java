package com.insa.h4112.view.cartographic;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.model.algorithm.BasicPair;
import com.insa.h4112.model.domain.Delivery;
import com.insa.h4112.model.domain.TourRequest;
import com.insa.h4112.model.domain.Warehouse;

import java.awt.*;
import java.util.HashMap;

/**
 * Class that display a tour request on map (cartographic view)
 *
 * @author Pierre-Yves GENEST
 */
public class TourRequestCartographicView implements View {

    /**
     * Current tour request
     */
    private TourRequest tourRequest;

    /**
     * All deliveries of this tour request
     */
    private HashMap<Delivery, DeliveryCartographicView> deliveryCartographicViewHashMap;

    /**
     * Warehouse of this tour request
     */
    private BasicPair<Warehouse, WarehouseCartographicView> warehouseCartographicViewBasicPair;

    /**
     * Checkpoint manager
     */
    private CheckpointCartographicViewFactory checkpointManager;

    /**
     * Constructor
     */
    public TourRequestCartographicView() {
        this.tourRequest = null;
        deliveryCartographicViewHashMap = new HashMap<>();
        warehouseCartographicViewBasicPair = new BasicPair<>(null, null);
    }

    /**
     * Set tour request
     *
     * @param tourRequest         new tour request
     * @param mapCartographicView mapview to find intersections
     * @param controller          Controller.
     */
    public void setTourRequest(TourRequest tourRequest, MapCartographicView mapCartographicView, Controller controller) {
        this.tourRequest = tourRequest;
        deliveryCartographicViewHashMap.clear();
        warehouseCartographicViewBasicPair.setFirst(null);
        warehouseCartographicViewBasicPair.setSecond(null);
        checkpointManager = new CheckpointCartographicViewFactory(controller);
        if (tourRequest != null) {
            // Creat warehouse
            warehouseCartographicViewBasicPair.setFirst(tourRequest.getWarehouse());
            warehouseCartographicViewBasicPair.setSecond(new WarehouseCartographicView(tourRequest.getWarehouse(), mapCartographicView, checkpointManager));

            // Create deliveries
            int i = 0;
            for (Delivery delivery : tourRequest.getDeliveries()) {
                deliveryCartographicViewHashMap.put(delivery, new DeliveryCartographicView(delivery, mapCartographicView, checkpointManager));
            }
        }
    }

    /**
     * To paint tour request on screen
     *
     * @param graphics2D graphics element
     * @param unit
     */
    @Override
    public void paint(Graphics2D graphics2D, double unit) {
        // Paint deliveries and warehouse
        for (DeliveryCartographicView deliveryCartographicView : deliveryCartographicViewHashMap.values()) {
            deliveryCartographicView.paint(graphics2D, unit);
        }

        if (warehouseCartographicViewBasicPair.getSecond() != null) {
            warehouseCartographicViewBasicPair.getSecond().paint(graphics2D, unit);
        }
    }

    /**
     * Click on coordinates
     *
     * @param coordinates click coordinates
     */
    public void click(Point.Double coordinates) {
        if (checkpointManager != null) {
            checkpointManager.select(coordinates);
        }
    }

    /**
     * To get checkpoint manager
     *
     * @return checkpoint manager
     */
    public CheckpointCartographicViewFactory getCheckpointManager() {
        return checkpointManager;
    }
}
