package com.insa.h4112.view;

import javax.swing.*;
import java.awt.*;

/**
 * Window containing the Roadmap as text and two buttons.
 *
 * @author Martin FRANCESCHI
 */
public class RoadmapWindow extends JDialog {

    /**
     * The text area containing the Roadmap text.
     */
    private JTextArea roadmapTextArea = new JTextArea();

    /**
     * The button for the "copy to clipboard" action.
     */
    private JButton copyToClipboardButton = new JButton("Copier");

    /**
     * The button for the "export to file" action.
     */
    private JButton exportButton = new JButton("Exporter...");

    /**
     * Constructor. Generates the window.
     * The user must fill the text area, add listeners to the buttons and turn visible.
     *
     * @param owner The RoadmapWindow's owner frame.
     */
    public RoadmapWindow(Frame owner) {
        super(owner, "Feuille de route");
        this.getContentPane().setLayout(new BorderLayout());

        JScrollPane scrollPaneForTextArea = new JScrollPane(roadmapTextArea);
        roadmapTextArea.setLineWrap(true);
        roadmapTextArea.setEditable(true);
        roadmapTextArea.setWrapStyleWord(true);
        this.getContentPane().add(scrollPaneForTextArea, BorderLayout.CENTER);

        JPanel buttonsPane = new JPanel();
        buttonsPane.setLayout(new GridLayout(1, 2));
        buttonsPane.add(copyToClipboardButton, 0);
        buttonsPane.add(exportButton, 1);
        this.getContentPane().add(buttonsPane, BorderLayout.SOUTH);

        // Set size
        DisplayMode screenSize = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();
        Dimension windowSize = new Dimension(
                (int) (screenSize.getWidth() * .75),
                (int) (screenSize.getHeight() * .65));
        this.setSize(windowSize);

        // Set location
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Point centerPoint = ge.getCenterPoint();
        int dx = centerPoint.x - windowSize.width / 2;
        int dy = centerPoint.y - windowSize.height / 2;
        setLocation(dx, dy);

        // Set other settings
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE); // Hide and clear up resources
        this.setResizable(true);
        this.setModalityType(ModalityType.APPLICATION_MODAL);
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            // Put the scroll bar of the text area at top.
            roadmapTextArea.setSelectionStart(0);
            roadmapTextArea.setSelectionEnd(0);
        }
        super.setVisible(visible);
    }

    /**
     * @return the JTextArea.
     */
    public JTextArea getRoadmapTextArea() {
        return roadmapTextArea;
    }

    /**
     * @return The button "copy to clipboard".
     */
    public JButton getCopyToClipboardButton() {
        return copyToClipboardButton;
    }


    /**
     * @return The button "export".
     */
    public JButton getExportButton() {
        return exportButton;
    }
}
