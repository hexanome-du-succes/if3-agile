package com.insa.h4112.view.legend;

import javax.swing.*;
import java.awt.*;

/**
 * Map legend explaining the meaning of different shapes used in the cartographic view
 *
 * @author Thomas Zhou
 */
public class LegendView extends JPanel {

    /**
     * The JTable containing the explanations
     */
    private JTable legendTable;

    /**
     * Renderer for the JTable
     */
    private final LegendShapeCellRenderer SHAPE_CELL_RENDERER = new LegendShapeCellRenderer();

    public LegendView() {
        super();
        setLayout(new FlowLayout(FlowLayout.LEADING));
        setPreferredSize(new Dimension(0, 70));
        setBorder(BorderFactory.createTitledBorder("Légende"));
        setVisible(false);

        legendTable = new JTable();
        LegendTableModel model = new LegendTableModel();
        legendTable.setModel(model);
        SHAPE_CELL_RENDERER.setHorizontalAlignment(JLabel.CENTER);
        legendTable.setDefaultRenderer(Object.class, SHAPE_CELL_RENDERER);
        legendTable.setRowSelectionAllowed(false);

        for (int i = 0; i < legendTable.getColumnCount(); i++) {
            if (i % 2 == 0) {
                //Shape cell
                legendTable.getColumnModel().getColumn(i).setPreferredWidth(40);
            } else {
                //Description cell
                legendTable.getColumnModel().getColumn(i).setPreferredWidth(120);
            }
        }
        legendTable.setRowHeight(30);

        add(legendTable);
    }

}
