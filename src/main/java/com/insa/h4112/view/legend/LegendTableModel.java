package com.insa.h4112.view.legend;


import javax.swing.table.AbstractTableModel;

/**
 * Model for the legend JTable in {@link LegendView}
 *
 * @author Thomas Zhou
 */
public class LegendTableModel extends AbstractTableModel {

    /**
     * List of all the shapes with respective description
     */
    private LegendShapeDescription shapeDescription;

    /**
     * Columns name
     */
    private static final String[] columnsName = {"Forme", "Signification", "Forme", "Signification", "Forme", "Signification"};

    /**
     * Constructor
     */
    public LegendTableModel() {
        shapeDescription = new LegendShapeDescription();
    }

    /**
     * Get column name
     *
     * @param columnIndex column index
     * @return column name
     */
    @Override
    public String getColumnName(int columnIndex) {
        return columnsName[columnIndex];
    }

    /**
     * Get total number of rows.
     *
     * @return 1 the table contains only one row since it is horizontally displayed
     */
    @Override
    public int getRowCount() {
        return 1;
    }

    /**
     * Get total number of columns
     *
     * @return total number of columns
     */
    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    /**
     * Get value of a cell
     *
     * @param rowIndex    index of row
     * @param columnIndex index of column
     * @return value in this cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return shapeDescription.getValueAt(columnIndex);
    }
}
