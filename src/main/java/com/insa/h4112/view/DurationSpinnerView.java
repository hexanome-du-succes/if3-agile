package com.insa.h4112.view;

import javax.swing.*;
import java.awt.*;
import java.time.Duration;

public class DurationSpinnerView extends JPanel {

    private JSpinner jSpinner;

    private static final Long VAL = 5L;

    private static final Long MAX = 100L;

    private static final Long MIN = 0L;

    private static final Long STEP = 1L;

    private JLabel beforeJLabel;

    private JLabel afterJLabel;

    public DurationSpinnerView() {
        super(new FlowLayout());
        beforeJLabel = new JLabel();
        afterJLabel = new JLabel();
        jSpinner = new JSpinner(new SpinnerNumberModel(VAL, MIN, MAX, STEP));

        this.add(beforeJLabel, BorderLayout.WEST);
        this.add(jSpinner, BorderLayout.CENTER);
        this.add(afterJLabel, BorderLayout.EAST);

        setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
    }

    public void setBeforeSpinnerString(String beforeSpinnerString) {
        beforeJLabel.setText(beforeSpinnerString);
    }

    public void setAfterSpinnerString(String afterSpinnerString) {
        afterJLabel.setText(afterSpinnerString);
    }

    public void resetSpinner() {
        jSpinner.setModel(new SpinnerNumberModel(VAL, MIN, MAX, STEP));
    }

    public Duration getDuration() {
        return Duration.ofMinutes((Long) jSpinner.getValue());
    }
}
