package com.insa.h4112.view.util;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

/**
 * Factory to create special shape displayed in the cartographic view
 *
 * @author Thomas Zhou
 */
public class ShapeViewFactory {

    /**
     * Deleted constructor
     */
    private ShapeViewFactory() {
    }

    /**
     * Create the shape for an intersection
     *
     * @param x1 the X coordinate of the start point
     * @param y1 the Y coordinate of the start point
     * @param x2 the X coordinate of the end point
     * @param y2 the Y coordinate of the end point
     * @return Shape {@code Line2D}
     */

    public static Shape createSectionShape(double x1, double y1, double x2, double y2) {
        return new Line2D.Double(x1, y1, x2, y2);
    }

    /**
     * Create the shape for an intersection
     *
     * @param x the X coordinate of the upper-left corner
     *          of the framing rectangle
     * @param y the Y coordinate of the upper-left corner
     *          of the framing rectangle
     * @param w the width of the framing rectangle
     * @param h the height of the framing rectangle
     * @return Shape {@code Ellipse2D}
     */
    public static Shape createIntersectionShape(double x, double y, double w, double h) {
        return new Ellipse2D.Double(x, y, w, h);
    }

    /**
     * Create the shape for a pick-up checkpoint
     *
     * @param x the X coordinate of the upper-left corner
     *          of the framing rectangle
     * @param y the Y coordinate of the upper-left corner
     *          of the framing rectangle
     * @param w the width of the framing rectangle
     * @param h the height of the framing rectangle
     * @return Shape {@code Ellipse2D}
     */
    public static Shape createPickUpShape(double x, double y, double w, double h) {
        return new Ellipse2D.Double(x, y, w, h);
    }

    /**
     * Create the shape for a deliver checkpoint
     *
     * @param x the X coordinate of the upper-left corner
     *          of the newly constructed {@code Rectangle2D}
     * @param y the Y coordinate of the upper-left corner
     *          of the newly constructed {@code Rectangle2D}
     * @param w the width of the newly constructed
     *          {@code Rectangle2D}
     * @param h the height of the newly constructed
     *          {@code Rectangle2D}
     * @return Shape {@code Rectangle2D}
     */
    public static Shape createDeliverShape(double x, double y, double w, double h) {
        return new Rectangle2D.Double(x, y, w, h);
    }


    /**
     * Create the shape for a warehouse checkpoint
     *
     * @param x the X coordinate of the upper-left corner
     *          of the newly constructed {@code Polygon}
     * @param y the Y coordinate of the upper-left corner
     *          of the newly constructed {@code Polygon}
     * @param w the width of the newly constructed
     *          {@code Polygon}
     * @param h the height of the newly constructed
     *          {@code Polygon}
     * @return Shape {@code Rectangle2D}
     */
    public static Shape createWarehouseShape(double x, double y, double w, double h) {
        int[] xPoints = {(int) (x + w / 2), (int) x, (int) (x + w)};
        int[] yPoints = {(int) y, (int) (y + h), (int) (y + h)};
        int nPoints = 3;
        return new Polygon(xPoints, yPoints, nPoints);
    }

    /**
     * Update a pickup shape
     *
     * @param shape pickup shape (ellipse2D)
     * @param x     the X coordinate of the upper-left corner
     *              of the framing rectangle
     * @param y     the Y coordinate of the upper-left corner
     *              of the framing rectangle
     * @param w     the width of the framing rectangle
     * @param h     the height of the framing rectangle
     * @return Shape {@code Ellipse2D}
     */
    public static void updatePickUpShape(Shape shape, double x, double y, double w, double h) {
        Ellipse2D.Double delivery = (Ellipse2D.Double) shape;
        assert (delivery != null);

        delivery.x = x;
        delivery.y = y;
        delivery.width = w;
        delivery.height = h;
    }

    /**
     * Update a deliver shape
     *
     * @param shape deliver shape (Rectangle2D)
     * @param x     the X coordinate of the upper-left corner
     *              of the framing rectangle
     * @param y     the Y coordinate of the upper-left corner
     *              of the framing rectangle
     * @param w     the width of the framing rectangle
     * @param h     the height of the framing rectangle
     * @return Shape {@code Rectangle2D}
     */
    public static void updateDeliverShape(Shape shape, double x, double y, double w, double h) {
        Rectangle2D.Double pickUp = (Rectangle2D.Double) shape;
        assert (pickUp != null);

        pickUp.x = x;
        pickUp.y = y;
        pickUp.width = w;
        pickUp.height = h;
    }

    /**
     * Update a warehouse shape
     *
     * @param shape warehouse shape (Polygon)
     * @param x     the X coordinate of the upper-left corner
     *              of the framing rectangle
     * @param y     the Y coordinate of the upper-left corner
     *              of the framing rectangle
     * @param w     the width of the framing rectangle
     * @param h     the height of the framing rectangle
     * @return Shape {@code Polygon}
     */
    public static void updateWarehouseShape(Shape shape, double x, double y, double w, double h) {
        Polygon warehouse = (Polygon) shape;
        assert (warehouse != null && warehouse.npoints == 3);

        // Change xPoints
        warehouse.xpoints[0] = (int) (x + w / 2);
        warehouse.xpoints[1] = (int) x;
        warehouse.xpoints[2] = (int) (x + w);
        // Update xPoints
        warehouse.ypoints[0] = (int) y;
        warehouse.ypoints[1] = (int) (y + h);
        warehouse.ypoints[2] = (int) (y + h);

        // Update polygon
        warehouse.invalidate();
    }

    /**
     * Update a pickup shape
     *
     * @param shape pickup shape (ellipse2D)
     * @param x     the X coordinate of the upper-left corner
     *              of the framing rectangle
     * @param y     the Y coordinate of the upper-left corner
     *              of the framing rectangle
     * @param w     the width of the framing rectangle
     * @param h     the height of the framing rectangle
     * @return Shape {@code Ellipse2D}
     */
    public static void updateIntersectionShape(Shape shape, double x, double y, double w, double h) {
        Ellipse2D.Double intersection = (Ellipse2D.Double) shape;
        assert (intersection != null);

        intersection.x = x;
        intersection.y = y;
        intersection.width = w;
        intersection.height = h;
    }
}
