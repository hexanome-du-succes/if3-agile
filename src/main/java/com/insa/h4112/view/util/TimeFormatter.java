package com.insa.h4112.view.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Date;

/**
 * Centralizes time to string conversion methods.
 */
public interface TimeFormatter {

    /**
     * Date formatter for string generation.
     */
    DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Hour and minutes formatter for string generation.
     */
    DateTimeFormatter TIME_FORMATTER_WITHOUT_SECONDS = DateTimeFormatter.ofPattern("HH'h'mm");

    /**
     * @param time the date to output.
     * @return a string of format "dd/MM/yyyy".
     */
    static String getShort(Date time) {
        return DATE_FORMAT.format(time);
    }

    /**
     * @param timePoint the clock time to output.
     * @return a string of format "hh:mm".
     */
    static String getShort(LocalTime timePoint) {
        return timePoint.format(TIME_FORMATTER_WITHOUT_SECONDS);
    }

    /**
     * @param beginning The start point.
     * @param end       The end point.
     * @return the duration between both Temporal instances.
     */
    static String getLiteral(Temporal beginning, Temporal end) {
        return getLiteral(Duration.between(beginning, end));
    }

    /**
     * @param duration The duration to describe.
     * @return the given duration as string.
     */
    static String getLiteral(Duration duration) {
        if (duration.getSeconds() < 60) {
            return "< 1 minute";
        } else {
            int hours = duration.toHoursPart();
            int minutes = duration.toMinutesPart();

            return (getDurationPart(hours, "heure", "heures") +
                    getDurationPart(minutes, "minute", "minutes")).trim();
        }
    }

    /**
     * @param millis milliseconds of duration.
     * @return a string of format "1.23 secondes"
     */
    static String getSecondsLiteral(long millis) {
        double seconds = (double) millis / 1000.;
        return String.format(
                "%s %s%s",
                new DecimalFormat("#0.00").format(seconds),
                "seconde",
                seconds <= 1. ? "" : "s"
        );
    }

    /**
     * Returns, as a string the given duration-part number and a unit.
     * If number is 0 then nothing is print at all.
     * If number is 1 then singular is print after.
     * Any other value -> plural is print after.
     *
     * @param number   Number to print.
     * @param singular singular string of the number's unit.
     * @param plural   plural string of the number's unit.
     * @return the resulting string.
     */
    static String getDurationPart(int number, String singular, String plural) {
        if (number > 0) {
            return String.format(
                    "%d %s ",
                    number,
                    ((number == 1) ? singular : plural));
        } else {
            return "";
        }
    }


}
