package com.insa.h4112.view.util;

import java.awt.*;

/**
 * Manage application colors
 *
 * @author Pierre-Yves GENEST
 */
public class Colors {

    /**
     * Color when checkpoint is selected
     */
    public static final Color CHECKPOINT_SELECTED_COLOR = new Color(21, 101, 192);

    /**
     * Color when checkpoint is associated to a selection
     */
    public static final Color CHECKPOINT_ASSOCIATED_SELECTED_COLOR = new Color(100, 181, 246);

    /**
     * Color when mapview need to be faded
     */
    public static final Color FADED_MAPVIEW_COLOR = new Color(255, 255, 255, 200);

    /**
     * Background of the logger pane.
     * Source:
     */
    public static final Color LOG_PANE_BACKGROUND = new Color(0xEEEEEE);

    /**
     * Color for the warning output on the logger.
     * Source: "darkorange" on https://www.rapidtables.com/web/color/orange-color.html.
     */
    public static final Color LOG_WARN_ORANGE = new Color(0xFF8C00);

    /**
     * Transparent color
     */
    public static final Color TRANSPARENT = new Color(0, 0, 0, 0);

    /**
     * Cycle of hue to generate color (20 colors per cycle)
     */
    private static final double HUE_CYCLE = 20.;

    /**
     * Generate random color
     *
     * @param seed seed to generate color
     * @return generated color
     */
    public static Color generateColor(int seed) {
        float hue = (float) (1.5 + (seed / HUE_CYCLE) % 1 + 2 * (Math.pow(0.5, seed / (int) HUE_CYCLE) - 1) / HUE_CYCLE) % 1;
        float saturation = 0.9f;
        float luminance = 0.9f - (float) ((seed / (int) HUE_CYCLE) / 10.) * 0.2f;
        return Color.getHSBColor(hue, saturation, luminance);
    }

    /**
     * Generate random color (around green)
     *
     * @param seed seed to generate color
     * @return generated color
     */
    public static Color generateRed(int seed) {
        float hue = 0.00f + (float) (Math.cos(seed / Math.PI)) * 0.03f;
        float saturation = 0.9f;
        float luminance = 1f - (float) (Math.abs((seed / Math.PI) % 1)) * 0.2f;
        return Color.getHSBColor(hue, saturation, luminance);
    }

    /**
     * Deleted constructor
     */
    private Colors() {
    }
}
