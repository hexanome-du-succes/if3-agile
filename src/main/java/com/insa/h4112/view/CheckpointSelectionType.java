package com.insa.h4112.view;

/**
 * Type of selection for checkpoints
 *
 * @author Pierre-Yves GENEST
 */
public enum CheckpointSelectionType {
    NONE,           // Not selected
    SELECTED,       // Selected
    ASSOCIATED      // Associated to a selected checkpoint (other member of delivery/warehouse)
}
