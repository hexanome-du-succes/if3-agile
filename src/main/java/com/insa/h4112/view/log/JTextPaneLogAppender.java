package com.insa.h4112.view.log;

import com.insa.h4112.view.util.Colors;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.io.Serializable;

/**
 * JTextPane appender for log4j2.
 * /!\ Entirely taken from :
 * https://github.com/FairPlayer4/Log4j2Example/blob/master/src/main/java/textpane/TextPaneAppender.java
 * We only modified some colors.
 *
 * @author Martin FRANCESCHI
 */
@Plugin(
        name = "JTextPaneAppender",
        category = "Core",
        elementType = "appender",
        printObject = true)
public final class JTextPaneLogAppender extends AbstractAppender {

    private static JTextPane textPane;

    private static JScrollPane scrollPane;

    /**
     * Set TextArea to append
     *
     * @param textPane TextArea to append
     */
    public static void setTextPane(JTextPane textPane, JScrollPane scrollPane) {
        JTextPaneLogAppender.textPane = textPane;
        JTextPaneLogAppender.scrollPane = scrollPane;
        textPane.setEditable(false);
        textPane.setBackground(Colors.LOG_PANE_BACKGROUND);
    }

    private JTextPaneLogAppender(String name, Filter filter,
                                 Layout<? extends Serializable> layout,
                                 final boolean ignoreExceptions, Property[] properties) {
        super(name, filter, layout, ignoreExceptions, properties);
    }

    /**
     * Factory method. Log4j will parse the configuration and call this factory
     * method to construct the appender with
     * the configured attributes.
     *
     * @param name   Name of appender
     * @param layout Log layout of appender
     * @param filter Filter for appender
     * @return The TextAreaAppender
     */
    @PluginFactory
    public static JTextPaneLogAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginElement("Properties") Property[] properties) {
        if (name == null) {
            LOGGER.error("No name provided for TextPaneAppender");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new JTextPaneLogAppender(name, filter, layout, true, properties);
    }

    /**
     * This method is where the appender does the work.
     *
     * @param event Log event with log data
     */
    @Override
    public void append(LogEvent event) {
        String message = new String(getLayout().toByteArray(event));

        // append log text to TextArea
        try {
            if (event.getLevel().equals(Level.INFO)) {
                appendOnTextPane(message, Color.BLACK);
            } else if (event.getLevel().equals(Level.WARN)) {
                appendOnTextPane(message, Colors.LOG_WARN_ORANGE);
            } else if (event.getLevel().equals(Level.ERROR)) {
                appendOnTextPane(message, Color.RED);
            } else if (event.getLevel().equals(Level.forName("SUCC", 350))) {
                appendOnTextPane(message, Color.GREEN.darker());
            }
        } catch (Exception e) {
            // Do not log exceptions that were caused by logging.
            e.printStackTrace();
        }
    }

    private void appendOnTextPane(String msg, Color color) {
        SwingUtilities.invokeLater(() -> {
            if (textPane != null) {
                StyledDocument doc = textPane.getStyledDocument();

                Style style = textPane.addStyle("ConsoleStyle", null);
                StyleConstants.setForeground(style, color);
                StyleConstants.setFontSize(style, 12);

                try {
                    doc.insertString(doc.getLength(), msg, style);
                    scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
                } catch (BadLocationException e) {
                    //e.printStackTrace();
                }
            }
        });
    }
}
