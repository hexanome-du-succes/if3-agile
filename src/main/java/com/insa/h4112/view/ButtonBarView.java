package com.insa.h4112.view;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.model.domain.TourRequest;

import javax.swing.*;
import java.awt.*;

/**
 * View class containing all the buttons for user interactions with the application
 *
 * @author Paul Goux
 * @author Thomas Zhou
 */
public class ButtonBarView extends JPanel {

    public final static String OPTIMIZE_BUTTON_TEXT_NOT_OPTIMIZED = "Calculer la tournée";
    public final static String[] OPTIMIZE_BUTTON_TEXTS_COMPUTING = {
            "Calcul en cours",
            "Calcul en cours.",
            "Calcul en cours..",
            "Calcul en cours..."
    };
    public final static String OPTIMIZE_BUTTON_TEXT_FINISHED = "Tournée calculée";

    /**
     * Button to load a {@link Map}
     */
    private JButton loadMapButton;

    /**
     * Button to load a {@link TourRequest}
     */
    private JButton loadTourRequestButton;

    /**
     * Button to optimize {@link Tour}
     */
    private JButton computeTourButton;

    /**
     * Button to generate a road map
     */
    private JButton obtainRoadmapButton;

    /**
     * The controller to manage global application
     */
    private Controller controller = null;

    /**
     * Add action listener to each button of the button bar
     */
    private void initButtonsActions() {
        loadMapButton.addActionListener(e -> controller.loadMapEvent());
        loadTourRequestButton.addActionListener(e -> controller.loadTourRequestEvent());
        computeTourButton.addActionListener(e -> controller.optimizeTourEvent());
        obtainRoadmapButton.addActionListener(e -> controller.obtainRoadmapEvent());
    }

    /**
     * Change the state of a button. A button is either enabled or disabled.
     *
     * @param enable If true the button is enabled. If false the button is disabled.
     */
    void setLoadMapButtonEnable(boolean enable) {
        loadMapButton.setEnabled(enable);
    }

    void setLoadTourRequestButtonEnable(boolean enable) {
        loadTourRequestButton.setEnabled(enable);
    }

    void setComputeTourButtonEnable(boolean enable) {
        computeTourButton.setEnabled(enable);
    }

    void setObtainRoadmapButtonEnable(boolean enable) {
        obtainRoadmapButton.setEnabled(enable);
    }

    void setComputeTourButtonText(String text) {
        computeTourButton.setText(text);
    }

    /**
     * Constructor
     */
    ButtonBarView() {
        super();
        setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        // Create buttons
        loadMapButton = new JButton("Charger un plan");
        add(loadMapButton);
        loadTourRequestButton = new JButton("Charger la demande de tournée");
        add(loadTourRequestButton);
        computeTourButton = new JButton(OPTIMIZE_BUTTON_TEXT_NOT_OPTIMIZED);
        add(computeTourButton);
        obtainRoadmapButton = new JButton("Afficher la feuille de route");
        add(obtainRoadmapButton);

        // Fix size of Compute Tour Button
        SwingUtilities.invokeLater(() -> {
            while (computeTourButton.getSize().height == 0) {
                Thread.yield(); // Wait for GUI to be up-to-date.
            }
            computeTourButton.setPreferredSize(computeTourButton.getSize());
        });
    }

    /**
     * Setter
     *
     * @param controller controller to use
     */
    public void setController(Controller controller) {
        this.controller = controller;
        if (controller != null) {
            initButtonsActions();
        }
    }
}
