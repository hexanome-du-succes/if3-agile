package com.insa.h4112.view.roadmap;

import com.insa.h4112.model.domain.*;

public abstract class AbstractRoadmapFactory implements RoadmapFactory {

    /**
     * Current TourRequest to work with.
     * Assigned on each call of getRoadmap.
     */
    protected TourRequest tourRequest = null;

    @Override
    public abstract String getRoadmap(Tour theTour);

    /**
     * Prints the given text as a title of the given level.
     *
     * @param text The title text.
     */
    protected abstract void printTitle(String text);

    /**
     * Prints the given Tour's metadata: start time, end time, duration.
     *
     * @param leTour The tour to describe.
     */
    protected abstract void printMetadata(final Tour leTour);

    /**
     * Prints the given path and its sections details.
     *
     * @param path The path to describe.
     */
    protected abstract void printPathDetails(Path path);

    /**
     * Prints the given path's primary information.
     *
     * @param path The path to describe.
     */
    protected abstract void printPathMetadata(Path path);

    /**
     * Prints a dedicated message in case there is no path (only CP to CP).
     */
    protected abstract void printNoPathMessage();

    /**
     * @param intersection The intersection to describe.
     * @return a string of pattern "(lat=x, lon=y)" in floating point values.
     */
    String getIntersectionCoordinates(Intersection intersection) {
        return String.format(
                "(lat=%f, lon=%f)",
                intersection.getLatitude(),
                intersection.getLongitude());
    }

    /**
     * @param section the section
     * @return the section's street name, or a verbose value.
     */
    String getSectionName(Section section) {
        return section.getStreetName().equals("") ? "(tronçon sans nom)" : section.getStreetName();
    }

    /**
     * @param preciseDirection the given direction.
     * @return a string describing the action that the driver should do according to the direction.
     */
    String directionToString(PreciseDirection preciseDirection) {
        Direction direction = preciseDirection.getDirection();
        int exitNumber = preciseDirection.getExitNumber();

        switch (direction) {
            case LEFT:
                if (exitNumber == -1) {
                    return "Tournez à gauche. ";
                } else {
                    if (exitNumber == 1) {
                        return "Prenez la 1ère à gauche. ";
                    } else {
                        return "Prenez la " + exitNumber + "ème à gauche. ";
                    }
                }

            case AHEAD:
                return "Continuez tout droit. ";

            case RIGHT:
                if (exitNumber == -1) {
                    return "Tournez à droite. ";
                } else {
                    if (exitNumber == 1) {
                        return "Prenez la 1ère à droite. ";
                    } else {
                        return "Prenez la " + exitNumber + "ème à droite. ";
                    }
                }

            default:
                throw new IllegalArgumentException();
        }
    }
}
