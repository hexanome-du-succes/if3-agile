package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.controller.command.ComputeOptimizedTourCommand;
import com.insa.h4112.controller.command.LoadMapCommand;
import com.insa.h4112.controller.command.LoadTourRequestCommand;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.TourRequest;
import com.insa.h4112.view.ButtonBarView;
import com.insa.h4112.view.MainWindow;

/**
 * tour request state
 *
 * @author Pierre-Yves GENEST
 */
public class TourRequestState implements State {
    /**
     * Map managed by state
     */
    private Map map;

    /**
     * Tour request managed by state
     */
    private TourRequest tourRequest;

    /**
     * Algorithm to use when optimizing tour
     */
    private TourCompleteGraph tourCompleteGraph;

    /**
     * Constructor
     *
     * @param map         map
     * @param tourRequest tour request managed by this state
     */
    public TourRequestState(Map map, TourRequest tourRequest, TourCompleteGraph tourCompleteGraph) {
        this.map = map;
        this.tourRequest = tourRequest;
        this.tourCompleteGraph = tourCompleteGraph;
    }

    /**
     * Enter state
     * => set up state
     *
     * @param controller controller
     */
    @Override
    public void enter(Controller controller) {
        controller.setMap(map);
        controller.setTourRequest(tourRequest);
        controller.setTourCompleteGraph(tourCompleteGraph);
        MainWindow mainWindow = controller.getMainWindow();
        mainWindow.setLoadMapButtonEnable(true);
        mainWindow.setLoadTourRequestButtonEnable(true);
        mainWindow.setComputeTourButtonEnable(true);
        mainWindow.setComputeTourButtonText(ButtonBarView.OPTIMIZE_BUTTON_TEXT_NOT_OPTIMIZED);
    }

    /**
     * Leave state
     * => cleaning for next state
     *
     * @param controller controller
     */
    @Override
    public void leave(Controller controller) {
        controller.setTourRequest(null);
        controller.setTourCompleteGraph(null);
        MainWindow mainWindow = controller.getMainWindow();
        mainWindow.setLoadMapButtonEnable(false);
        mainWindow.setLoadTourRequestButtonEnable(false);
        mainWindow.setComputeTourButtonEnable(false);
    }

    /**
     * Event to load a tour request
     * load a new tour request
     *
     * @param controller Controller
     * @return command to load a tour
     */
    @Override
    public Command loadTourRequestEvent(Controller controller) throws CommandException {
        return new LoadTourRequestCommand(controller, this);
    }

    /**
     * Event to load a map
     * Reload a new map
     *
     * @param controller Controller
     * @return command to load a map
     */
    @Override
    public Command loadMapEvent(Controller controller) throws CommandException {
        return new LoadMapCommand(controller, this);
    }

    /**
     * Event to compute/optimize tour
     *
     * @param controller Controller
     * @return command to optimize tour
     */
    @Override
    public Command optimizeTourEvent(Controller controller) {
        return new ComputeOptimizedTourCommand(controller, this);
    }

    /**
     * When a checkpoint is selected
     *
     * @param controller        Controller
     * @param clickedCheckpoint clicked checkpoint
     * @return null
     */
    @Override
    public Command clickOnCheckpointEvent(Controller controller, Checkpoint clickedCheckpoint) {
        // No change state
        controller.selectCheckpoints(clickedCheckpoint);
        return null;
    }

    /**
     * TO escape checkpoint selection
     *
     * @param controller Controller
     * @return null
     */
    @Override
    public Command escapeKeyEvent(Controller controller) {
        controller.selectCheckpoints(null);
        return null;
    }
}
