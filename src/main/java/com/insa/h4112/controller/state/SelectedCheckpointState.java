package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.AddingDeliveryEnterProcessCommand;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.controller.command.DeleteDeliveryCommand;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.model.domain.TourRequest;

/**
 * State when a checkpoint is selected
 *
 * @author Pierre-Yves GENEST
 */
public class SelectedCheckpointState extends OptimizedTourState {

    /**
     * Selected checkpoint
     */
    private Checkpoint selectedCheckpoint;

    /**
     * Previous state (in case of undo/redo)
     * we do not want to return in selectedCheckpointState
     */
    private State previousState;

    /**
     * Enter state
     *
     * @param controller controller
     */
    @Override
    public void enter(Controller controller) {
        super.enter(controller);
        controller.selectCheckpointEvent(selectedCheckpoint);
    }

    /**
     * Leave state
     *
     * @param controller controller
     */
    @Override
    public void leave(Controller controller) {
        super.leave(controller);
        controller.selectCheckpoints(null);
        controller.getMainWindow().setDeleteCheckpointButtonEnable(false);
    }

    /**
     * Constructor
     *
     * @param map                map
     * @param tourRequest        tour request
     * @param tourCompleteGraph  tour complete graph
     * @param tour               tour
     * @param selectedCheckpoint selected checkpoint
     * @param previousState      previous state (in case of undo/redo)
     */
    public SelectedCheckpointState(Map map, TourRequest tourRequest, TourCompleteGraph tourCompleteGraph, Tour tour, Checkpoint selectedCheckpoint, State previousState) {
        super(map, tourRequest, tourCompleteGraph, tour);
        this.selectedCheckpoint = selectedCheckpoint;
        this.previousState = previousState;
    }

    /**
     * When a checkpoint is selected
     *
     * @param controller        Controller
     * @param clickedCheckpoint clicked checkpoint
     * @return null
     */
    @Override
    public Command clickOnCheckpointEvent(Controller controller, Checkpoint clickedCheckpoint) {
        selectedCheckpoint = clickedCheckpoint;
        if (isWarehouse(controller, clickedCheckpoint))
            controller.getMainWindow().setDeleteCheckpointButtonEnable(false);
        else
            controller.getMainWindow().setDeleteCheckpointButtonEnable(true);
        controller.selectCheckpoints(clickedCheckpoint);
        return null;
    }

    /**
     * When escape is pressed
     *
     * @param controller Controller
     * @return null
     */
    @Override
    public Command escapeKeyEvent(Controller controller) {
        controller.changeState(new OptimizedTourState(map, tourRequest, tourCompleteGraph, tour));
        return null;
    }

    /**
     * Event when delete key is pressed
     *
     * @param controller Controller
     * @return null
     */
    @Override
    public Command deleteKeyEvent(Controller controller) {
        return deleteCheckpointEvent(controller);
    }

    /**
     * Event when delete button is pressed
     *
     * @param controller Controller
     * @return null
     */
    @Override
    public Command deleteCheckpointEvent(Controller controller) {
        if (isWarehouse(controller)) {
            return null;
        }
        return new DeleteDeliveryCommand(controller, previousState);
    }

    /**
     * Test if the current selected checkpoint is the Warehouse (begin or end) in the tour request in the given controller
     *
     * @param controller controller to test with
     * @return true if it's warehouse, false otherwise
     */
    private boolean isWarehouse(Controller controller) {
        return isWarehouse(controller.getTourRequest(), selectedCheckpoint);
    }

    /**
     * Test if the checkpoint is the Warehouse (begin or end) of the given tour request
     *
     * @param tourRequest tour request to test
     * @param checkpoint  checkpoint to test
     * @return true if it's warehouse, false otherwise
     */
    private boolean isWarehouse(TourRequest tourRequest, Checkpoint checkpoint) {
        return checkpoint.equals(tourRequest.getWarehouse().getStart()) || checkpoint.equals(tourRequest.getWarehouse().getEnd());
    }

    /**
     * Test if the checkpoint is the Warehouse (begin or end) in the tour request in the given controller
     *
     * @param controller controller to test with
     * @param checkpoint checkpoint to test if it's the wawrehouse
     * @return true if it's warehouse, false otherwise
     */
    private boolean isWarehouse(Controller controller, Checkpoint checkpoint) {
        return isWarehouse(controller.getTourRequest(), checkpoint);
    }

    @Override
    public Command addDeliveryEvent(Controller controller) {
        return new AddingDeliveryEnterProcessCommand(controller, this);
    }
}
