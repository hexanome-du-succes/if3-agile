package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.TourRequest;
import com.insa.h4112.view.ButtonBarView;
import com.insa.h4112.view.MainWindow;

import javax.swing.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * State during the calculation of the optimized tour
 *
 * @author Martin FRANCESCHI
 */
public class OptimizingTourState implements State {
    /**
     * The map
     */
    private Map map;

    /**
     * The tour request
     */
    private TourRequest tourRequest;

    /**
     * The algorithm to use when optimizing tour
     */
    private TourCompleteGraph tourCompleteGraph;

    /**
     * The main view window
     */
    private MainWindow mainWindow = null;

    /**
     * Tells whether the Swing Worker should keep running.
     */
    private final AtomicBoolean shouldTextChangingWorkerContinue = new AtomicBoolean(true);

    /**
     * Main constructor
     *
     * @param map               The map.
     * @param tourRequest       The tour request.
     * @param tourCompleteGraph The algorithm to use when optimizing tour
     */
    public OptimizingTourState(Map map, TourRequest tourRequest, TourCompleteGraph tourCompleteGraph) {
        this.map = map;
        this.tourRequest = tourRequest;
        this.tourCompleteGraph = tourCompleteGraph;
    }

    @Override
    public void enter(Controller controller) {
        mainWindow = controller.getMainWindow();

        controller.setMap(map);
        controller.setTourRequest(tourRequest);
        controller.setTourCompleteGraph(tourCompleteGraph);

        mainWindow.setLoadMapButtonEnable(false);
        mainWindow.setLoadTourRequestButtonEnable(false);
        mainWindow.setComputeTourButtonEnable(false);
        mainWindow.setGetPathDetailsButtonEnable(false);
        // No text in Compute Tour Button: it is handled in the corresponding command.

        // Start a Swing Worker for periodically update the GUI.
        shouldTextChangingWorkerContinue.set(true);
        SwingWorker<Void, Void> textChangingWorker = new ComputeButtonTextChanger();
        textChangingWorker.execute();
    }

    @Override
    public void leave(Controller controller) {
        controller.setTourRequest(null);
        controller.setTourCompleteGraph(null);
        controller.getMainWindow().setLoadMapButtonEnable(true);
        controller.getMainWindow().setLoadTourRequestButtonEnable(true);
        controller.getMainWindow().setComputeTourButtonEnable(false);

        // Shuts down the Swing Worker.
        shouldTextChangingWorkerContinue.set(false);
    }

    /**
     * When a checkpoint is selected
     *
     * @param controller        Controller
     * @param clickedCheckpoint clicked checkpoint
     * @return null
     */
    @Override
    public Command clickOnCheckpointEvent(Controller controller, Checkpoint clickedCheckpoint) {
        // No change state
        controller.selectCheckpoints(clickedCheckpoint);
        return null;
    }

    /**
     * Class that periodically updates the GUI: the Compute Text Button.
     * This Swing Worker works on an AWT-specific thread and thus can work on the GUI.
     */
    private class ComputeButtonTextChanger extends SwingWorker<Void, Void> {

        @Override
        protected Void doInBackground() {
            int textIndex = 0;

            while (shouldTextChangingWorkerContinue.get()) {
                mainWindow.setComputeTourButtonText(ButtonBarView.OPTIMIZE_BUTTON_TEXTS_COMPUTING[textIndex]);
                textIndex = (++textIndex) % ButtonBarView.OPTIMIZE_BUTTON_TEXTS_COMPUTING.length;

                // Wait during half a second.
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ignored) {
                }
            }
            return null;
        }
    }
}
