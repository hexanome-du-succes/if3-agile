package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.AddDeliveryCommand;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.*;
import com.insa.h4112.util.log.Log;

/**
 * Final step for adding a delivery, the user needs to choose the checkpoint of the tour to go by before the deliver checkpoint chosen
 *
 * @author Paul GOUX
 * @author Thomas ZHOU
 */
public class AddingDeliveryDeliveryChosenState implements State {
    /**
     * The map
     */
    private Map map;

    /**
     * The tour
     */
    private Tour tour;

    /**
     * The tour request
     */
    private TourRequest tourRequest;

    /**
     * Algorithm to use when optimizing tour
     */
    private TourCompleteGraph tourCompleteGraph;

    /**
     * The pickup checkpoint of the delivery to add.
     */
    private Checkpoint pickupCheckpoint;

    /**
     * The chosen checkpoint to go by before the pickup checkpoint of the delivery to add.
     */
    private Checkpoint checkpointBeforePickup;

    /**
     * The intersection chosen to be the deliver checkpoint of the delivery to add.
     */
    private Intersection deliveryIntersection;

    /**
     * Main constructor
     *
     * @param map                    The map.
     * @param tour                   The tour.
     * @param tourRequest            The tour request.
     * @param tourCompleteGraph      The algorithm used when optimizing the tour.
     * @param pickupCheckpoint       The pickup checkpoint of the delivery to add.
     * @param checkpointBeforePickup The checkpoint to go by before the pickup checkpoint of the delivery to add.
     * @param deliveryIntersection   The intersection chosen to be the deliver checkpoint of the delivery to add.
     */
    public AddingDeliveryDeliveryChosenState(Map map, Tour tour, TourRequest tourRequest, TourCompleteGraph tourCompleteGraph, Checkpoint pickupCheckpoint, Checkpoint checkpointBeforePickup, Intersection deliveryIntersection) {
        this.map = map;
        this.tour = tour;
        this.tourRequest = tourRequest;
        this.tourCompleteGraph = tourCompleteGraph;
        this.pickupCheckpoint = pickupCheckpoint;
        this.checkpointBeforePickup = checkpointBeforePickup;
        this.deliveryIntersection = deliveryIntersection;
    }

    @Override
    public void enter(Controller controller) {
        controller.setMap(map);
        controller.setTourRequest(tourRequest);
        controller.setTour(tour);
        controller.setTourCompleteGraph(tourCompleteGraph);

        controller.selectIntersection(deliveryIntersection);
        controller.selectIntersection(pickupCheckpoint.getIntersection());
        controller.highlightCheckpoint(checkpointBeforePickup);

        Log.info(Log.INFO_SELECT_BEFORE_DELIVER);
    }

    @Override
    public void leave(Controller controller) {
        controller.setTourRequest(null);
        controller.setTour(null);
        controller.setTourCompleteGraph(null);

        controller.unselectIntersection(deliveryIntersection);
        controller.unselectIntersection(pickupCheckpoint.getIntersection());
        controller.unHighlightCheckpoint(checkpointBeforePickup);
    }

    /**
     * Event when the user choose the checkpoint to go by before the deliver checkpoint of the delivery to add
     *
     * @param controller               Controller
     * @param checkpointBeforeDelivery the checkpoint before the deliver checkpoint of the delivery to add
     * @return command to add the delivery to the tour
     */
    @Override
    public Command clickOnCheckpointEvent(Controller controller, Checkpoint checkpointBeforeDelivery) throws CommandException {
        Checkpoint deliveryCheckpoint = new Checkpoint(controller.getMainWindow().getDurationInSpinner(), deliveryIntersection);
        Delivery newDelivery = new Delivery(pickupCheckpoint, deliveryCheckpoint);
        return new AddDeliveryCommand(controller, this, newDelivery, checkpointBeforePickup, checkpointBeforeDelivery);
    }

    /**
     * Event when escape key is pressed
     *
     * @param controller Controller
     * @return null
     */
    @Override
    public Command escapeKeyEvent(Controller controller) {
        Log.info(Log.INFO_CANCEL_ADDING_DELIVER);
        controller.popHistory();
        controller.changeState(new OptimizedTourState(map, tourRequest, tourCompleteGraph, tour));
        return null;
    }
}
