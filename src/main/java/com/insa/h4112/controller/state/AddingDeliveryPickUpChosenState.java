package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.AddingDeliveryChooseBeforePickUpCommand;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.*;
import com.insa.h4112.util.log.Log;

/**
 * Second step for adding a delivery, the user needs to choose the checkpoint of the tour to go by before the pickup checkpoint chosen
 *
 * @author Paul GOUX
 * @author Thomas ZHOU
 */
public class AddingDeliveryPickUpChosenState implements State {

    /**
     * The map
     */
    private Map map;

    /**
     * The tour
     */
    private Tour tour;

    /**
     * The tour request
     */
    private TourRequest tourRequest;

    /**
     * Algorithm to use when optimizing tour
     */
    private TourCompleteGraph tourCompleteGraph;

    /**
     * The intersection chosen to be the pickup checkpoint of the delivery to add.
     */
    private Intersection pickupIntersection;

    public AddingDeliveryPickUpChosenState(Map map, Tour tour, TourRequest tourRequest, TourCompleteGraph tourCompleteGraph, Intersection pickupIntersection) {
        this.map = map;
        this.tour = tour;
        this.tourRequest = tourRequest;
        this.tourCompleteGraph = tourCompleteGraph;
        this.pickupIntersection = pickupIntersection;
    }

    @Override
    public void enter(Controller controller) {
        controller.setMap(map);
        controller.setTourRequest(tourRequest);
        controller.setTour(tour);
        controller.setTourCompleteGraph(tourCompleteGraph);
        controller.selectIntersection(pickupIntersection);

        Log.info(Log.INFO_SELECT_BEFORE_PICKUP);
    }

    @Override
    public void leave(Controller controller) {
        controller.setTourRequest(null);
        controller.setTour(null);
        controller.setTourCompleteGraph(null);
        controller.unselectIntersection(pickupIntersection);
    }

    /**
     * Event when the user clicks on one checkpoint of the tour
     *
     * @param controller             Controller
     * @param checkpointBeforePickup the checkpoint to go by before the pickup checkpoint of delivery to add
     * @return
     */
    @Override
    public Command clickOnCheckpointEvent(Controller controller, Checkpoint checkpointBeforePickup) {
        return new AddingDeliveryChooseBeforePickUpCommand(controller, this, pickupIntersection, checkpointBeforePickup, controller.getMainWindow().getDurationInSpinner());
    }

    /**
     * Event when escape key is pressed
     *
     * @param controller Controller
     * @return null
     */
    @Override
    public Command escapeKeyEvent(Controller controller) {
        Log.info(Log.INFO_CANCEL_ADDING_DELIVER);
        controller.popHistory();
        controller.changeState(new OptimizedTourState(map, tourRequest, tourCompleteGraph, tour));
        return null;
    }
}
