package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.AddingDeliveryChoosePickUpCommand;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.model.domain.TourRequest;
import com.insa.h4112.util.log.Log;

/**
 * First step for adding a delivery, the user needs to choose the pickup checkpoint
 *
 * @author Paul GOUX
 * @author Thomas ZHOU
 */
public class AddingDeliveryInitState implements State {
    /**
     * The map of the application
     */
    private Map map;

    /**
     * The tour of the application
     */
    private Tour tour;

    /**
     * The tour request of the application
     */
    private TourRequest tourRequest;

    /**
     * Algorithm to use when optimizing tour
     */
    private TourCompleteGraph tourCompleteGraph;

    /**
     * Main constructor
     *
     * @param map               The map of the application.
     * @param tour              The tour of the application.
     * @param tourRequest       The tour request of the application.
     * @param tourCompleteGraph The algorithm to use when optimizing tour
     */
    public AddingDeliveryInitState(Map map, Tour tour, TourRequest tourRequest, TourCompleteGraph tourCompleteGraph) {
        this.map = map;
        this.tour = tour;
        this.tourRequest = tourRequest;
        this.tourCompleteGraph = tourCompleteGraph;
    }

    @Override
    public void enter(Controller controller) {
        controller.setMap(map);
        controller.setTourRequest(tourRequest);
        controller.setTour(tour);
        controller.setTourCompleteGraph(tourCompleteGraph);
        controller.resetDurationSpinner();
        controller.displayPickUpDuration();

        Log.info(Log.INFO_SELECT_PICKUP);
    }

    @Override
    public void leave(Controller controller) {
        controller.setTourRequest(null);
        controller.setTour(null);
        controller.setTourCompleteGraph(null);
        controller.setVisibleSpinnerDuration(false);
    }

    /**
     * Event when an intersection is clicked to be pickup checkpoint of the delivery to add
     *
     * @param controller         controller of the application
     * @param pickupIntersection the intersection chosen to be the pickup checkpoint
     * @return command to add the intersection
     */
    @Override
    public Command clickIntersectionEvent(Controller controller, Intersection pickupIntersection) throws CommandException {
        return new AddingDeliveryChoosePickUpCommand(controller, this, pickupIntersection);
    }

    /**
     * Event when escape key is pressed
     *
     * @param controller Controller
     * @return null
     */
    @Override
    public Command escapeKeyEvent(Controller controller) {
        Log.info(Log.INFO_CANCEL_ADDING_DELIVER);
        controller.popHistory();
        controller.changeState(new OptimizedTourState(map, tourRequest, tourCompleteGraph, tour));
        return null;
    }
}
