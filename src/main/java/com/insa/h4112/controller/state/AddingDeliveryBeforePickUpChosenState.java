package com.insa.h4112.controller.state;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.AddingDeliveryChooseDeliverCommand;
import com.insa.h4112.controller.command.Command;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.*;
import com.insa.h4112.util.log.Log;

/**
 * Third step for adding a delivery, the user needs to choose the deliver checkpoint
 *
 * @author Paul GOUX
 * @author Thomas ZHOU
 */
public class AddingDeliveryBeforePickUpChosenState implements State {
    /**
     * The map
     */
    private Map map;

    /**
     * The tour
     */
    private Tour tour;

    /**
     * The tour request
     */
    private TourRequest tourRequest;

    /**
     * The algorithm used when optimizing the tour request
     */
    private TourCompleteGraph tourCompleteGraph;

    /**
     * The pickup checkpoint of the delivery to add.
     */
    private Checkpoint pickupCheckpoint;

    /**
     * The chosen checkpoint to go by before the pickup checkpoint of the delivery to add.
     */
    private Checkpoint checkpointBeforePickup;

    /**
     * Main constructor
     *
     * @param map                    The map.
     * @param tour                   The tour.
     * @param tourRequest            The tour request.
     * @param tourCompleteGraph      The algorithm used when optimizing the tour.
     * @param pickupCheckpoint       The pickup checkpoint of the delivery to add.
     * @param checkpointBeforePickup The checkpoint to go by before the pickup checkpoint of the delivery to add.
     */
    public AddingDeliveryBeforePickUpChosenState(Map map, Tour tour, TourRequest tourRequest, TourCompleteGraph tourCompleteGraph, Checkpoint pickupCheckpoint, Checkpoint checkpointBeforePickup) {
        this.map = map;
        this.tour = tour;
        this.tourRequest = tourRequest;
        this.tourCompleteGraph = tourCompleteGraph;
        this.pickupCheckpoint = pickupCheckpoint;
        this.checkpointBeforePickup = checkpointBeforePickup;
    }

    @Override
    public void enter(Controller controller) {
        controller.setMap(map);
        controller.setTourRequest(tourRequest);
        controller.setTour(tour);
        controller.setTourCompleteGraph(tourCompleteGraph);
        controller.resetDurationSpinner();
        controller.displayDeliverDuration();
        controller.highlightCheckpoint(checkpointBeforePickup);
        controller.selectIntersection(pickupCheckpoint.getIntersection());

        Log.info(Log.INFO_SELECT_DELIVER);
    }

    @Override
    public void leave(Controller controller) {
        controller.setTourRequest(null);
        controller.setTour(null);
        controller.setTourCompleteGraph(null);
        controller.setVisibleSpinnerDuration(false);
        controller.unHighlightCheckpoint(checkpointBeforePickup);
        controller.unselectIntersection(pickupCheckpoint.getIntersection());
    }

    /**
     * Event when the user selects the intersection which will be the deliver checkpoint of the delivery to add
     *
     * @param controller           controller
     * @param deliveryIntersection
     * @return
     * @throws CommandException
     */
    @Override
    public Command clickIntersectionEvent(Controller controller, Intersection deliveryIntersection) throws CommandException {
        return new AddingDeliveryChooseDeliverCommand(controller, this, pickupCheckpoint, checkpointBeforePickup, deliveryIntersection);
    }

    /**
     * Event when escape key is pressed
     *
     * @param controller Controller
     * @return null
     */
    @Override
    public Command escapeKeyEvent(Controller controller) {
        Log.info(Log.INFO_CANCEL_ADDING_DELIVER);
        controller.popHistory();
        controller.changeState(new OptimizedTourState(map, tourRequest, tourCompleteGraph, tour));
        return null;
    }
}
