package com.insa.h4112.controller;

import com.insa.h4112.controller.command.Command;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.controller.command.util.CommandHistory;
import com.insa.h4112.controller.command.util.StackCommandHistory;
import com.insa.h4112.controller.state.InitialState;
import com.insa.h4112.controller.state.State;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.*;
import com.insa.h4112.util.Procedure;
import com.insa.h4112.util.log.Log;
import com.insa.h4112.view.MainWindow;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.time.Duration;

/**
 * Controller of application
 * MVC Design Pattern
 *
 * @author Pierre-Yves GENEST
 */
public class Controller implements PropertyChangeListener {

    /* ----- CONTROLLER VARIABLES ----- */
    /**
     * Controller's current state.
     */
    private State currentState = null;

    /**
     * Command History: manages the history
     */
    private CommandHistory history = new StackCommandHistory();

    /**
     * Algorithm to use when optimizing tour
     */
    private TourCompleteGraph tourCompleteGraph = null;

    /* ----- VIEW VARIABLES ----- */
    /**
     * The application's view window.
     */
    private MainWindow mainWindow = null;

    /* ----- MODEL VARIABLES ----- */
    /**
     * The current map.
     */
    private Map map = null;
    /**
     * The current tour request.
     */
    private TourRequest tourRequest = null;
    /**
     * The current tour.
     */
    private Tour tour = null;

    /* ----- Selected checkpoint/delivery ----- */
    /**
     * Selected checkpoint
     */
    private Checkpoint selectedCheckpoint = null;
    /**
     * Second selected checkpoint (other member of delivery or warehouse)
     */
    private Checkpoint associatedSelectedCheckpoint = null;

    /**
     * The constructor.
     */
    public Controller() {
    }

    /**
     * Set the main window of the application
     *
     * @param mainWindow window to use
     */
    public void setMainWindow(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void setInitialState() {
        currentState = new InitialState();
        currentState.enter(this);

        Log.controllerState(currentState);
    }

    public TourCompleteGraph getTourCompleteGraph() {
        return tourCompleteGraph;
    }

    public void setTourCompleteGraph(TourCompleteGraph tourCompleteGraph) {
        this.tourCompleteGraph = tourCompleteGraph;
    }

    /**
     * @return The application's main window.
     */
    public MainWindow getMainWindow() {
        return mainWindow;
    }

    /**
     * Change state of controller.
     *
     * @param newState The new state to enter in.
     */
    public void changeState(State newState) {
        // Leave old state
        currentState.leave(this);

        // Enter new state
        currentState = newState;
        currentState.enter(this);

        Log.controllerState(currentState);
    }

    ////////// Command list //////////

    /**
     * Add a command to command list AND execute it.
     *
     * @param command command to add.
     */
    private void addCommand(Command command) {
        history.addCommand(command);
    }

    /**
     * Event to undo previous command
     *
     * @return true if a command was undone
     */
    public boolean undoEvent() {
        return history.undoCommand();
    }

    /**
     * Event to redo next command
     *
     * @return true if a command was redone
     */
    public boolean redoEvent() {
        return history.redoCommand();
    }

    ////////// EVENTS //////////

    /**
     * Event to load a map
     */
    public void loadMapEvent() {
        handleStateEventCall(() -> addCommand(currentState.loadMapEvent(this)));
    }

    /**
     * Event to load a tour request
     */
    public void loadTourRequestEvent() {
        handleStateEventCall(() -> addCommand(currentState.loadTourRequestEvent(this)));
    }

    /**
     * Event to compute/optimize tour
     */
    public void optimizeTourEvent() {
        handleStateEventCall(() -> addCommand(currentState.optimizeTourEvent(this)));
    }

    /**
     * Reorder tour event
     */
    public void reorderTourEvent(int from, int to) {
        handleStateEventCall(() -> addCommand(currentState.moveCheckpointEvent(this, from, to, tour)));
    }

    /**
     * Event to obtain Roadmap.
     */
    public void obtainRoadmapEvent() {
        handleStateEventCall(() -> currentState.obtainRoadmapEvent(this));
    }

    /**
     * Event to obtain Durations (while adding a delivery).
     */
    public void obtainDurationsEvent(Duration pickupDuration, Duration deliveryDuration) {
        handleStateEventCall(() -> addCommand(currentState.obtainDurationsEvent(this)));
    }

    /**
     * Event when a checkpoint is selected
     *
     * @param checkpoint checkpoint to select
     */
    public void selectCheckpointEvent(Checkpoint checkpoint) {
        handleStateEventCall(() -> addCommand(currentState.clickOnCheckpointEvent(this, checkpoint)));
    }

    /**
     * Event when a intersection is selected
     *
     * @param intersection intersection to select
     */
    public void selectIntersectionEvent(Intersection intersection) {
        handleStateEventCall(() -> addCommand(currentState.clickIntersectionEvent(this, intersection)));
    }

    /**
     * Event called when escape key is pressed
     */
    public void escapeKeyEvent() {
        handleStateEventCall(() -> currentState.escapeKeyEvent(this));
    }

    /**
     * Event called when delete touch is pressed
     */
    public void deleteKeyEvent() {
        handleStateEventCall(() -> addCommand(currentState.deleteKeyEvent(this)));
    }

    /**
     * To call state event call securely
     *
     * @param eventCall event call
     */
    private void handleStateEventCall(Procedure<CommandException> eventCall) {
        try {
            eventCall.run();
        } catch (CommandException e) {
            Log.errorCommand(e.getMessage());
        }
    }

    /**
     * Event when "delete delivery" checkpoint is pressed
     */
    public void deleteSelectedCheckpointEvent() {
        handleStateEventCall(() -> addCommand(currentState.deleteCheckpointEvent(this)));
    }

    /**
     * Event when "add delivery" button is pressed
     */
    public void addDeliveryEvent() {
        handleStateEventCall(() -> addCommand(currentState.addDeliveryEvent(this)));
    }

    ////////// GETTERS/SETTERS //////////

    /**
     * Get current controller's map
     *
     * @return current map
     */
    public Map getMap() {
        return map;
    }

    /**
     * Get current controller's tour request
     *
     * @return current tour request
     */
    public TourRequest getTourRequest() {
        return tourRequest;
    }

    /**
     * Get current controller's tour
     *
     * @return current tour
     */
    public Tour getTour() {
        return tour;
    }

    /**
     * Set current controller's map
     *
     * @param map new map
     */
    public void setMap(Map map) {
        this.map = map;
        mainWindow.setMap(map);
    }

    /**
     * Set current controller's tour request
     *
     * @param tourRequest new tour request
     */
    public void setTourRequest(TourRequest tourRequest) {
        this.tourRequest = tourRequest;
        selectCheckpoints(null);

        mainWindow.setTourRequest(tourRequest);
    }

    /**
     * Set current controller's tour
     *
     * @param tour new tour
     */
    public void setTour(Tour tour) {
        this.tour = tour;
        mainWindow.setTour(tour);
    }

    /**
     * Return the current selected checkpoint
     *
     * @return the current selected checkpoint
     */
    public Checkpoint getSelectedCheckpoint() {
        return selectedCheckpoint;
    }

    /**
     * Select a checkpoint (and its delivery)
     *
     * @param checkpoint selected checkpoint
     */
    public void selectCheckpoints(Checkpoint checkpoint) {
        // unselect previous selected checkpoint
        if (selectedCheckpoint != null) {
            mainWindow.unselectCheckpoints(selectedCheckpoint, associatedSelectedCheckpoint);
        }

        // select new selected checkpoint
        this.selectedCheckpoint = checkpoint;

        if (selectedCheckpoint == null) {
            // Remove associated checkpoint
            associatedSelectedCheckpoint = null;

        } else {
            // Else find associated checkpoint
            associatedSelectedCheckpoint = tourRequest.getOtherCheckpointInDelivery(selectedCheckpoint);

            // Select checkpoint and associated checkpoint
            mainWindow.selectCheckpoints(selectedCheckpoint, associatedSelectedCheckpoint);
        }
    }

    /**
     * Add a new history (for sharper undo/redo)
     */
    public void addHistory() {
        history.addSubHistory();
    }

    /**
     * Delete current history
     */
    public void popHistory() {
        history.popSubHistory();
    }

    /**
     * Listen events
     *
     * @param evt event to listen
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case Events.SELECT_CHECKPOINT_EVENT:   // Checkpoint is selected
                selectCheckpointEvent((Checkpoint) evt.getNewValue());
                break;
            case Events.SELECT_INTERSECTION_EVENT:
                selectIntersectionEvent((Intersection) evt.getNewValue());
                break;
            default:
                break;
        }
    }

    /**
     * Display the JSpinner for inputting the duration when adding a delivery
     */
    public void displayDeliverDuration() {
        mainWindow.displayDeliverDuration();
    }

    /**
     * set visible or not the Spinner when adding a delivery
     *
     * @param visible if true, display the Spinner, hide it otherwise
     */
    public void setVisibleSpinnerDuration(boolean visible) {
        mainWindow.setVisibleSpinnerDuration(visible);
    }

    /**
     * Select an intersection to the view
     *
     * @param intersection intersection to select
     */
    public void selectIntersection(Intersection intersection) {
        mainWindow.selectIntersection(intersection);
    }

    /**
     * Unselect an intersection to the view
     *
     * @param intersection intersection to unselect
     */
    public void unselectIntersection(Intersection intersection) {
        mainWindow.unselectIntersection(intersection);
    }

    /**
     * Highlight a single checkpoint
     *
     * @param checkpoint checkpoint
     */
    public void highlightCheckpoint(Checkpoint checkpoint) {
        mainWindow.highlightCheckpoint(checkpoint);
    }

    /**
     * Unhighlight a single checkpoint
     *
     * @param checkpoint checkpoint
     */
    public void unHighlightCheckpoint(Checkpoint checkpoint) {
        mainWindow.unHighlightCheckpoint(checkpoint);
    }

    /**
     * reset the value of the spinner to the default value
     */
    public void resetDurationSpinner() {
        mainWindow.resetDurationSpinner();
    }

    /**
     * reset spinner and his texts for pickup duration input
     */
    public void displayPickUpDuration() {
        mainWindow.displayPickUpDuration();
    }
}
