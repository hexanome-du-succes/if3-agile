package com.insa.h4112.controller.command;

/**
 * Command base interface for Command design pattern.
 * An action instance must have a constructor which gather all required resources.
 * The action can then be performed (doCommand) or undone (undoCommand).
 *
 * @author Pierre-Yves GENEST
 * @author Jacques CHARNAY
 * @author Martin FRANCESCHI
 */
public interface Command {

    /**
     * Do command
     */
    void doCommand();

    /**
     * Undo command
     */
    void undoCommand();

    /**
     * To get name of Command (equivalent of toString)
     *
     * @return Command class name
     */
    default String name() {
        return getClass().getSimpleName();
    }
}
