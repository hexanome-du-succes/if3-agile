package com.insa.h4112.controller.command.other;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.util.log.Log;
import com.insa.h4112.view.RoadmapWindow;
import com.insa.h4112.view.roadmap.RoadmapFactory;
import com.insa.h4112.view.roadmap.RoadmapFactoryText;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Generates the roadmap string and opens the roadmap window.
 * When the window closes it is over.
 *
 * @author Martin FRANCESCHI
 */
public class ObtainRoadmap {

    /**
     * The app's controller.
     */
    private final Controller controller;

    /**
     * The RoadmapWindow. Unique to the program.
     */
    private RoadmapWindow roadmapWindow;

    /**
     * Only constructor.
     * Generates the roadmap text and the dedicated window.
     *
     * @param controller The app's controller.
     */
    public ObtainRoadmap(Controller controller) {
        this.controller = controller;

        roadmapWindow = new RoadmapWindow(controller.getMainWindow());

        RoadmapFactory factory = new RoadmapFactoryText();
        String roadmapString = factory.getRoadmap(controller.getTour());
        roadmapWindow.getRoadmapTextArea().setText(roadmapString);

        roadmapWindow.getCopyToClipboardButton().addActionListener(actionEvent -> copyRoadmapToClipboard(getString()));
        roadmapWindow.getExportButton().addActionListener(actionEvent -> exportRoadmapToFile(getString()));

        roadmapWindow.setVisible(true);
    }

    /**
     * Writes the Roadmap string to a file of the user's choice.
     *
     * @param roadmapString the Roadmap string.
     */
    private void exportRoadmapToFile(String roadmapString) {
        // Get the file name.
        File choosenFile = controller.getMainWindow().chooseAnyFile();
        if (choosenFile == null) {
            Log.errorCommand(Log.ERROR_COMMAND_NO_FILE_SELECTED);
            return;
        }

        // Write to this file.
        // Code adapted from https://stackoverflow.com/a/2885241/11996851.
        try (Writer writer = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(choosenFile),
                        StandardCharsets.UTF_8))) {
            writer.write(roadmapString);
            Log.successRoadmapExport(choosenFile);
        } catch (IOException ioe) {
            Log.failRoadmapExport(choosenFile, ioe.getMessage());
        }
    }

    /**
     * Copies the Roadmap string to the user's clipboard.
     *
     * @param roadmapString the roadmap string
     */
    private void copyRoadmapToClipboard(String roadmapString) {
        // Code from https://stackoverflow.com/a/6713290/11996851.
        StringSelection stringSelection = new StringSelection(roadmapString);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);

        Log.successRoadmapClipboard();
    }

    /**
     * @return the String of the roadmap.
     */
    private String getString() {
        return roadmapWindow.getRoadmapTextArea().getText();
    }
}
