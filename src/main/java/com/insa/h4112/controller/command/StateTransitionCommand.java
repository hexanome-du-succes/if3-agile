package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.state.State;

/**
 * A command of which one of the actions is a state change for the controller.
 *
 * @author Pierre-Yves GENEST
 * @author Martin FRANCESCHI
 * @author Jacques CHARNAY
 */
public abstract class StateTransitionCommand implements Command {
    /**
     * The controller.
     */
    protected Controller controller;

    /**
     * The previous state (for undo)
     */
    protected State previousState;

    /**
     * The target state (for do)
     */
    protected State targetState;

    /**
     * Main constructor.
     *
     * @param controller    The app's controller.
     * @param previousState The previous state.
     *                      /!\ Must initialize targetState
     */
    StateTransitionCommand(Controller controller, State previousState) {
        this.controller = controller;
        this.previousState = previousState;
    }

    /**
     * Do command
     * Transition to target state
     */
    @Override
    public void doCommand() {
        controller.changeState(targetState);
    }

    /**
     * Undo command
     * Transition to previous state
     */
    @Override
    public void undoCommand() {
        controller.changeState(previousState);
    }
}
