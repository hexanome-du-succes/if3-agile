package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.state.AddingDeliveryInitState;
import com.insa.h4112.controller.state.State;

/**
 * Command to begin the adding delivery process
 *
 * @author Paul GOUX
 * @author Thomas ZHOU
 */
public class AddingDeliveryEnterProcessCommand extends StateTransitionCommand {
    /**
     * Main constructor.
     *
     * @param controller    The app's controller.
     * @param previousState The previous state.
     */
    public AddingDeliveryEnterProcessCommand(Controller controller, State previousState) {
        super(controller, previousState);
        controller.addHistory();
        targetState = new AddingDeliveryInitState(controller.getMap(), controller.getTour(), controller.getTourRequest(), controller.getTourCompleteGraph());
    }

    @Override
    public void doCommand() {
        super.doCommand();
    }

    @Override
    public void undoCommand() {
        super.undoCommand();
    }
}
