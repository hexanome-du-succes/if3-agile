package com.insa.h4112.controller.command;

import com.insa.h4112.controller.Controller;
import com.insa.h4112.controller.command.util.CommandException;
import com.insa.h4112.controller.state.State;
import com.insa.h4112.controller.state.TourRequestState;
import com.insa.h4112.model.algorithm.TourCompleteGraph;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.TourRequest;
import com.insa.h4112.model.parser.TourRequestXMLParser;
import com.insa.h4112.util.log.Log;
import org.jdom2.JDOMException;

import java.io.File;
import java.io.IOException;

/**
 * This command changes the controller's state from any previous to a new Map State.
 *
 * @author Pierre-Yves GENEST
 * @author Martin FRANCESCHI
 */
public class LoadTourRequestCommand extends StateTransitionCommand {

    /**
     * Constructor. Reads and closes the file.
     *
     * @param controller    controller
     * @param previousState previous state
     */
    public LoadTourRequestCommand(Controller controller, State previousState) throws CommandException {
        super(controller, previousState);

        Map map = controller.getMap();

        // Load tour request
        File chosenFile = controller.getMainWindow().selectXMLFile();
        if (chosenFile == null) { // file is invalid
            throw new CommandException(Log.ERROR_COMMAND_NO_FILE_SELECTED);
        }

        try {
            TourRequest tourRequest = TourRequestXMLParser.parseTourRequest(chosenFile, map);
            TourCompleteGraph tourCompleteGraph = new TourCompleteGraph(map, tourRequest);
            if (!tourCompleteGraph.verifyAllCheckpointsReachableFromWarehouse(tourRequest.getWarehouse())) {
                throw new CommandException(Log.ERROR_COMMAND_INVALID_TOUR_REQUEST);
            }
            targetState = new TourRequestState(map, tourRequest, tourCompleteGraph);
            Log.successLoadTourRequest(chosenFile);
        } catch (JDOMException e) {
            throw new CommandException(Log.ERROR_COMMAND_XML_INVALID);
        } catch (IOException e) {
            throw new CommandException(Log.ERROR_COMMAND_XML_NOT_FOUND);
        }
    }
}
