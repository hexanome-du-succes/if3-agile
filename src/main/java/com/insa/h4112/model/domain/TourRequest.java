package com.insa.h4112.model.domain;

import com.insa.h4112.controller.Events;

import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Tour request : deliveries (pick-up and delivery point) and warehouse
 * also includes Tour (when computed)
 * (fr : Demande de tournee)
 *
 * @author Paul GOUX
 */
public class TourRequest {

    /**
     * Warehouse of Tour request (start and end of tour)
     */
    private Warehouse warehouse;

    /**
     * All deliveries (each delivery is composed of pick-up and delivery points)
     */
    private List<Delivery> deliveries;

    /**
     * Used to notify changes to view
     */
    private PropertyChangeSupport propertyChangeSupport;

    /**
     * Give the delivery associated to checkpoint
     * (to accelerate access)
     */
    private java.util.Map<Checkpoint, Delivery> deliveryCorrespondingToCheckpoint;

    /**
     * Constructor
     *
     * @param warehouse  warehouse
     * @param deliveries all deliveries
     */
    public TourRequest(Warehouse warehouse, List<Delivery> deliveries) {
        this(null, warehouse, deliveries);
    }

    /**
     * Complete constructor
     *
     * @param tour       tour
     * @param warehouse  warehouse
     * @param deliveries all deliveries
     */
    public TourRequest(Tour tour, Warehouse warehouse, List<Delivery> deliveries) {
        this.warehouse = warehouse;
        this.deliveries = deliveries;
        propertyChangeSupport = new PropertyChangeSupport(this);
        deliveryCorrespondingToCheckpoint = new HashMap<>();
        computeDeliveryCorrespondingToCheckpoint();
    }

    /**
     * Default constructor
     */
    public TourRequest() {
        this(null, null, new ArrayList<>());
    }

    /**
     * Add the pickup checkpoint and the the deliver checkpoint with the associated delivery
     */
    private void computeDeliveryCorrespondingToCheckpoint() {
        deliveryCorrespondingToCheckpoint = new HashMap<>();

        Checkpoint pickUp, deliver;
        for (Delivery delivery : deliveries) {
            pickUp = delivery.getPickUp();
            deliver = delivery.getDeliver();
            deliveryCorrespondingToCheckpoint.put(pickUp, delivery);
            deliveryCorrespondingToCheckpoint.put(deliver, delivery);
        }
    }

    /**
     * Get the other checkpoint in the same delivery
     *
     * @param c the checkpoint
     * @return the associated checkpoint
     */
    public Checkpoint getOtherCheckpointInDelivery(Checkpoint c) {
        Delivery delivery = deliveryCorrespondingToCheckpoint.get(c);
        if (delivery == null) {
            if (warehouse.getStart() == c) {
                return warehouse.getEnd();
            } else {
                return warehouse.getStart();
            }
        }
        if (c == delivery.getPickUp()) {
            return delivery.getDeliver();
        } else {
            return delivery.getPickUp();
        }
    }

    /**
     * Get the delivery containing the checkpoint
     *
     * @param c the checkpoint
     * @return Delivery the delivery of the checkpoint
     */
    public Delivery getAssociatedDelivery(Checkpoint c) {
        return deliveryCorrespondingToCheckpoint.get(c);
    }

    /**
     * @param c the checkpoint
     * @return CheckpointType the type of the checkpoint (ie. Warehouse, Pickup or Deliver)
     */
    public CheckpointType getCheckpointType(Checkpoint c) {
        Delivery delivery = deliveryCorrespondingToCheckpoint.get(c);
        if (delivery != null) {
            if (delivery.getPickUp() == c) {
                return CheckpointType.PICK_UP;
            }
            return CheckpointType.DELIVER;
        }
        return CheckpointType.WAREHOUSE;
    }

    /**
     * Add a delivery
     *
     * @param delivery new delivery
     */
    public void addDelivery(Delivery delivery) {
        deliveries.add(delivery);
        addDeliveryAcceleratorMap(delivery);
        propertyChangeSupport.firePropertyChange(Events.ADD_DELIVERY_FROM_TOUR_REQUEST_EVENT, null, this);
    }

    /**
     * Add the associated delivery to the map with the pickup checkpoint and the deliver checkpoint to easily get their associated delivery
     *
     * @param delivery the delivery
     */
    private void addDeliveryAcceleratorMap(Delivery delivery) {
        Checkpoint pickUp, deliver;
        pickUp = delivery.getPickUp();
        deliver = delivery.getDeliver();
        deliveryCorrespondingToCheckpoint.put(pickUp, delivery);
        deliveryCorrespondingToCheckpoint.put(deliver, delivery);
    }

    /**
     * Get a delivery by index
     *
     * @param index index of delivery.
     *              <b>index not checked -> can throw OutOfBoundException</b>
     * @return delivery at index
     */
    public Delivery getDelivery(int index) {
        return deliveries.get(index);
    }

    /**
     * Remove a delivery (comparing objects)
     *
     * @param delivery delivery to remove
     */
    public void removeDelivery(Delivery delivery) {
        deliveries.remove(delivery);
        removeDeliveryAcceleratorMap(delivery);
        propertyChangeSupport.firePropertyChange(Events.REMOVE_DELIVERY_FROM_TOUR_REQUEST_EVENT, null, this);
    }

    /**
     * Remove a delivery by index
     *
     * @param index index of delivery
     *              <b>index not checked -> can throw OutOfBoundException</b>
     */
    public void removeDelivery(int index) {
        Delivery delivery = deliveries.get(index);
        removeDeliveryAcceleratorMap(delivery);
        deliveries.remove(index);
    }

    /**
     * Remove the pickup checkpoint and deliver checkpoint in the map
     *
     * @param delivery
     */
    private void removeDeliveryAcceleratorMap(Delivery delivery) {
        Checkpoint pickUp, deliver;
        pickUp = delivery.getPickUp();
        deliver = delivery.getDeliver();
        deliveryCorrespondingToCheckpoint.remove(pickUp);
        deliveryCorrespondingToCheckpoint.remove(deliver);
    }

    /**
     * Get all deliveries
     *
     * @return all deliveries
     */
    public List<Delivery> getDeliveries() {
        return deliveries;
    }

    /**
     * Accessor
     *
     * @return warehouse
     */
    public Warehouse getWarehouse() {
        return warehouse;
    }

    /**
     * Setter
     *
     * @param warehouse warehouse
     */
    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }

    /**
     * Test equality
     *
     * @param o object to compare
     * @return true if the specified object is equal to this Tour Request
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TourRequest that = (TourRequest) o;
        return Objects.equals(warehouse, that.warehouse) &&
                Objects.equals(deliveries, that.deliveries);
    }

    /**
     * Hash checkpoint
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(warehouse, deliveries);
    }

    /**
     * Description of object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "TourRequest{" +
                "warehouse=" + warehouse +
                ", deliveries=" + deliveries +
                '}';
    }
}
