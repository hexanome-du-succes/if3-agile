package com.insa.h4112.model.domain;

import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;

/**
 * Map containing intersections and sections
 * reminder : sections are contained in intersections (outgoingSections)
 * (fr : Plan)
 *
 * @author Paul GOUX
 */
public class Map {

    /**
     * All intersections in map
     * map = (id, intersection)
     */
    private java.util.Map<Long, Intersection> intersections;

    /**
     * Default constructor
     */
    public Map() {
        intersections = new HashMap<>();
    }

    /**
     * Copy constructor
     *
     * @param intersections intersections
     */
    public Map(java.util.Map<Long, Intersection> intersections) {
        this.intersections = intersections;
    }

    /**
     * Return Intersection identified by his <b>intersectionId</b>.
     *
     * @param intersectionId intersectionId of the {@link Intersection} to return
     * @return Intersection identified by intersectionId, null if there is not {@link Intersection} of this intersectionId.
     */
    public Intersection getIntersection(long intersectionId) {
        return intersections.get(intersectionId);
    }

    /**
     * Add intersection to the Map. If an intersection with the same id exist, it replace the old one.
     *
     * @param intersection {@link Intersection} to add
     */
    public void addIntersection(Intersection intersection) {
        intersections.put(intersection.getId(), intersection);
    }

    /**
     * remove the {@link Intersection} with the specified id.
     *
     * @param intersectionId id of the {@link Intersection} to remove.
     */
    public void removeIntersection(long intersectionId) {
        intersections.remove(intersectionId);
    }

    /**
     * Return all {@link Intersection} in the Map.
     *
     * @return all {@link Intersection}s in the Map.
     */
    public Collection<Intersection> getAllIntersectionsAsList() {
        return intersections.values();
    }

    /**
     * Test equality
     *
     * @param o object to compare
     * @return true if the specified object is equal to this Map
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Map map = (Map) o;
        return Objects.equals(intersections, map.intersections);
    }

    /**
     * Hash checkpoint
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(intersections);
    }

    /**
     * Description of object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "Map{" +
                "intersections=" + intersections +
                '}';
    }
}
