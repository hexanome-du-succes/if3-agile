package com.insa.h4112.model.domain;

import java.time.LocalTime;
import java.util.Objects;

/**
 * Warehouse (start and end of tour)
 * (fr : Entrepot)
 *
 * @author Paul GOUX
 */
public class Warehouse {

    /**
     * Checkpoint associated with tour start
     */
    private Checkpoint start;

    /**
     * Checkpoint associated with tour end
     */
    private Checkpoint end;

    /**
     * Complete constructor
     *
     * @param startTime start time of tour
     * @param start     start checkpoint
     * @param end       end checkpoint
     */
    public Warehouse(LocalTime startTime, Checkpoint start, Checkpoint end) {
        this.start = start;
        start.setPassageTime(startTime);    // Setting up start time of tour

        this.end = end;
    }

    /**
     * Accessor
     *
     * @return start checkpoint
     */
    public Checkpoint getStart() {
        return start;
    }

    /**
     * Setter
     *
     * @param start start checkpoint
     */
    public void setStart(Checkpoint start) {
        this.start = start;
    }

    /**
     * Accessor
     *
     * @return end checkpoint
     */
    public Checkpoint getEnd() {
        return end;
    }

    /**
     * Setter
     *
     * @param end end checkpoint
     */
    public void setEnd(Checkpoint end) {
        this.end = end;
    }

    /**
     * Test equality
     *
     * @param o object to compare
     * @return true if the specified object is equal to this Warehouse
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Warehouse warehouse = (Warehouse) o;
        return Objects.equals(start, warehouse.start) &&
                Objects.equals(end, warehouse.end);
    }

    /**
     * Hash checkpoint
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    /**
     * Generate a seed for color generator
     * /!\ Use this to generate color
     *
     * @return seed
     */
    public int getSeed() {
        return (int) Math.min(start.getId(), end.getId());
    }

    /**
     * Description of object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "Warehouse{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
