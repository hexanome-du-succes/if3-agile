package com.insa.h4112.model.domain;

/**
 * Represents the precise direction to follow at an intersection
 *
 * @author Jacques CHARNAY
 */
public class PreciseDirection {

    /**
     * the direction
     */
    private Direction direction;

    /**
     * number of exit (ex : 2nd road)
     */
    private int exitNumber;

    /**
     * Constructor
     *
     * @param direction  direction (right, left ...)
     * @param exitNumber number of this exit in the specified direction
     */
    public PreciseDirection(Direction direction, int exitNumber) {
        this.direction = direction;
        this.exitNumber = exitNumber;
    }

    /**
     * return the direction
     *
     * @return direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * return the exit number
     *
     * @return exit number
     */
    public int getExitNumber() {
        return exitNumber;
    }
}
