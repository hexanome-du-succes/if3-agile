package com.insa.h4112.model.domain;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Objects;

/**
 * Represents a checkpoint
 * (fr : point de passage)
 *
 * @author Paul GOUX
 */
public class Checkpoint {
    /**
     * Id of the next constructed checkpoint
     */
    private static long nextId = 0;

    /**
     * Checkpoint's id
     * Arbitrarily defined by the parser. It is used to distinguish 2 checkpoints sharing the same intersection
     */
    private long id;

    /**
     * Scheduled passage time of deliverer
     */
    private LocalTime passageTime;

    /**
     * Duration of pick-up or delivery
     */
    private Duration duration;

    /**
     * Time interval to go to this checkpoint
     * Automatically computed when passageTime is set for the first time
     */
    private TimeInterval interval;

    /**
     * Intersection where checkpoint is located
     */
    private Intersection intersection;

    /**
     * Useful for the {@link Warehouse}
     *
     * @param intersection {@link Intersection} where the {@link Checkpoint} is located
     */
    public Checkpoint(Intersection intersection) {
        this.id = nextId++;
        this.intersection = intersection;
        this.duration = Duration.ZERO;
        // interval is null
    }

    /**
     * Useful when passageTime is not already calculated
     *
     * @param duration     duration at the checkpoint
     * @param intersection {@link Intersection} where the {@link Checkpoint} is located
     */
    public Checkpoint(Duration duration, Intersection intersection) {
        this.id = nextId++;
        this.duration = duration;
        this.intersection = intersection;
        // interval is null
    }

    /**
     * Complete constructor
     *
     * @param passageTime  passage Time
     * @param duration     duration of delivery or pick-up
     * @param intersection location of checkpoint
     */
    public Checkpoint(LocalTime passageTime, Duration duration, Intersection intersection) {
        this.id = nextId++;
        this.duration = duration;
        this.intersection = intersection;

        // Set pasage time and compute time interval
        setPassageTime(passageTime);
    }

    /**
     * Accessor
     *
     * @return passage time
     */
    public LocalTime getPassageTime() {
        return passageTime;
    }

    /**
     * Setter
     *
     * @param passageTime passage time
     */
    public void setPassageTime(LocalTime passageTime) {
        this.passageTime = passageTime;

        // Interval is set the first time passageTime is set
        if (interval == null) {
            interval = TimeInterval.computeInterval(passageTime);
        }
    }

    /**
     * Accessor
     *
     * @return duration of delivery or pick-up
     */
    public Duration getDuration() {
        return duration;
    }

    /**
     * Setter
     *
     * @param duration duration
     */
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    /**
     * Getter
     *
     * @return intersection where checkpoint is located
     */
    public Intersection getIntersection() {
        return intersection;
    }

    /**
     * Setter
     *
     * @param intersection intersection where checkpoint is located
     */
    public void setIntersection(Intersection intersection) {
        this.intersection = intersection;
    }

    /**
     * Getter
     *
     * @return arbitrary checkpoint's id
     */
    public long getId() {
        return id;
    }

    /**
     * Getter
     *
     * @return interval
     */
    public TimeInterval getInterval() {
        return interval;
    }

    /**
     * Description of object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "Checkpoint{" +
                "id=" + id +
                ", passageTime=" + passageTime +
                ", duration=" + duration +
                ", interval=" + interval +
                ", intersection=" + intersection +
                '}';
    }

    /**
     * Test equality
     *
     * @param o object to compare
     * @return true if the specified object is equal to this Checkpoint
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Checkpoint that = (Checkpoint) o;
        return id == that.id;
    }

    /**
     * Hash checkpoint
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
