package com.insa.h4112.model.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Intersection of road sections (contains all exiting sections)
 * (fr : Intersection)
 *
 * @author Paul GOUX
 */
public class Intersection {

    /**
     * Intersection's id (from XML document)
     */
    private long id;

    /**
     * Latitude of this intersection (~ y)
     */
    private double latitude;

    /**
     * Longitude of this intersection (~ x)
     */
    private double longitude;

    /**
     * All sections that are exiting from this intersection
     * (graph vocabulary : successors)
     */
    private List<Section> outgoingSections;

    /**
     * @param id        id of the intersection
     * @param latitude  latitude of the intersection
     * @param longitude longitude of the intersection
     */
    public Intersection(long id, double latitude, double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        outgoingSections = new ArrayList<>();
    }

    /**
     * @param id               id of the intersection
     * @param latitude         latitude of the intersection
     * @param longitude        longitude of the intersection
     * @param outgoingSections {@link Section} who are linked to this intersection
     */
    public Intersection(long id, double latitude, double longitude, List<Section> outgoingSections) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.outgoingSections = outgoingSections;
    }

    /**
     * Accessor
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Accessor
     *
     * @return latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Setter
     *
     * @param latitude latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * Accessor
     *
     * @return longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Setter
     *
     * @param longitude longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * Accessor
     *
     * @return outgoing sections
     */
    public List<Section> getOutgoingSections() {
        return outgoingSections;
    }

    /**
     * Setter
     *
     * @param outgoingSections outgoingSections
     */
    public void setOutgoingSections(List<Section> outgoingSections) {
        this.outgoingSections = outgoingSections;
    }

    /**
     * Add a new Section exiting the intersection
     *
     * @param section New section to add
     */
    public void addOutgoingSection(Section section) {
        outgoingSections.add(section);
    }

    /**
     * Test equality
     *
     * @param o object to compare
     * @return true if the specified object is equal to this Intersection
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Intersection that = (Intersection) o;
        return id == that.id;
    }

    /**
     * Hash checkpoint
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**
     * Description of object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "Intersection{" +
                "id=" + id +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", outgoingSections=" + outgoingSections +
                '}';
    }
}
