package com.insa.h4112.model.domain;

/**
 * @author Jacques CHARNAY
 */
public enum Direction {
    LEFT,
    AHEAD,
    RIGHT
}