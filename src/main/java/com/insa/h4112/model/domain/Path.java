package com.insa.h4112.model.domain;

import java.time.Duration;
import java.util.List;
import java.util.Objects;

/**
 * Path (all sections) to link an origin checkpoint to a destination one
 * (fr : Trajet)
 *
 * @author Paul GOUX
 */
public class Path {

    /**
     * Origin checkpoint
     */
    private Checkpoint origin;

    /**
     * Destination checkpoint
     */
    private Checkpoint destination;

    /**
     * Ordered list of sections that link origin to destination
     */
    private List<Section> steps;

    /**
     * Deliverer speed (constant) in m/s
     */
    private static final double DELIVERER_SPEED = 15.0 / 3.6;

    /**
     * Complete constructor
     *
     * @param origin      origin checkpoint
     * @param destination destination checkpoint
     * @param steps       ordered list of sections that link origin to destination
     */
    public Path(Checkpoint origin, Checkpoint destination, List<Section> steps) {
        this.origin = origin;
        this.destination = destination;
        this.steps = steps;
    }

    /**
     * Calculate the path duration (without taking into account any waiting times at the start and end checkpoints)
     *
     * @return the path duration, for a deliverer going at 15 km/h
     */
    public Duration getPathDuration() {
        double totalLength = 0;
        for (Section step : steps) {
            totalLength += step.getLength();
        }
        return Duration.ofSeconds((long) (totalLength / DELIVERER_SPEED));
    }

    /**
     * Accessor
     *
     * @return origin checkpoint
     */
    public Checkpoint getOrigin() {
        return origin;
    }

    /**
     * Accessor
     *
     * @return destination checkpoint
     */
    public Checkpoint getDestination() {
        return destination;
    }

    /**
     * Accessor
     *
     * @return sections to link origin to destination
     */
    public List<Section> getSteps() {
        return steps;
    }

    /**
     * Test equality
     *
     * @param o object to compare
     * @return true if the specified object is equal to this Path
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Path path = (Path) o;
        return origin.equals(path.origin) &&
                destination.equals(path.destination) &&
                steps.equals(path.steps);
    }

    /**
     * Hash checkpoint
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(origin, destination, steps);
    }

    /**
     * Description of object
     *
     * @return description
     */
    @Override
    public String toString() {
        return "Path{" +
                "origin=" + origin +
                ", destination=" + destination +
                ", steps=" + steps +
                '}';
    }
}
