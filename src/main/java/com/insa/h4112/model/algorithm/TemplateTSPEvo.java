package com.insa.h4112.model.algorithm;

import com.insa.h4112.model.domain.Checkpoint;
import com.insa.h4112.model.domain.Path;
import com.insa.h4112.model.domain.Tour;
import com.insa.h4112.model.domain.TourRequest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Template to resolve efficiently the TSP problem in order to build the most optimized Tour possible
 *
 * @author First Version from: Christine SOLNON - Modified by: Jacques CHARNAY
 */
public abstract class TemplateTSPEvo {

    /* ----- TSP ALGORITHM VARIABLES ----- */

    /**
     * number of nodes that we have to cross in the TSP problem
     */
    private int nbNodes;

    /**
     * The cost to go from any node to an other one
     */
    private double[][] cost;

    /**
     * for each node, gives the index of the node by which it must possibly be preceded
     * (because they belong to the same Delivery
     * and one is the pick-up checkpoint and the other the Deliver checkpoint)
     */
    private int[] predecessors;

    /**
     * The best Tour actually found during algorithm execution
     * (technically, the corresponding index sequence)
     */
    private Integer[] bestSolution;

    /**
     * The total cost of the best solution found
     */
    private double costBestSolution;

    /**
     * Indicates if the execution time limit of TSP algorithm has been reached or not
     */
    private boolean timeLimitReached = false;

    /* ----- BUILDING FINAL PATH VARIABLES ----- */

    /**
     * All the checkpoints that we have to cross
     */
    private List<Checkpoint> checkpoints;

    /**
     * All the paths to go from any checkpoint to an other one
     */
    private Map<BasicPair<Checkpoint, Checkpoint>, Path> paths;

    /**
     * the beginning checkpoint (= the warehouse)
     */
    private Checkpoint start;

    /**
     * the ending checkpoint (= the warehouse)
     */
    private Checkpoint end;

    /**
     * the tour complete graph
     */
    private TourCompleteGraph tourCompleteGraph;

    /**
     * tour request
     */
    private TourRequest tourRequest;

    /**
     * the Constructor, used to initialize all the data necessary for the execution of the TSP algorithm.
     * Mainly, it converts work objects into most efficient data structures, in this case simple arrays of integers
     *
     * @param tourRequest       the tour Request
     * @param tourCompleteGraph the complete checkpoint graph
     */
    public TemplateTSPEvo(TourRequest tourRequest, TourCompleteGraph tourCompleteGraph) {
        this.tourCompleteGraph = tourCompleteGraph;
        this.tourRequest = tourRequest;
        //We gather all the interesting data from tourCompleteGraph:
        checkpoints = new ArrayList<>(tourCompleteGraph.getCheckpoints());
        this.start = tourRequest.getWarehouse().getStart();
        this.end = tourRequest.getWarehouse().getEnd();
        checkpoints.remove(start);
        checkpoints.remove(end);
        Map<BasicPair<Checkpoint, Checkpoint>, Double> costCheckpoints = tourCompleteGraph.getDistances();
        Map<Checkpoint, Checkpoint> precededBy = tourCompleteGraph.getPrecededBy();
        paths = tourCompleteGraph.getPaths();

        costBestSolution = Double.MAX_VALUE;
        nbNodes = checkpoints.size() + 1;//All the deliveries steps + the warehouse
        cost = new double[checkpoints.size() + 1][checkpoints.size() + 1];

        //We convert the ineffective Checkpoint Map into an efficient 2d double array
        //To do this, each checkpoint is arbitrarily assigned an index
        cost[0][0] = 0.0;
        for (int j = 1; j <= checkpoints.size(); ++j) {
            cost[0][j] = costCheckpoints.get(new BasicPair<>(start, checkpoints.get(j - 1)));
        }
        for (int i = 1; i <= checkpoints.size(); ++i) {
            cost[i][0] = costCheckpoints.get(new BasicPair<>(checkpoints.get(i - 1), start));
            for (int j = 1; j <= checkpoints.size(); ++j) {
                cost[i][j] = costCheckpoints.get(new BasicPair<>(checkpoints.get(i - 1), checkpoints.get(j - 1)));
            }
        }

        //For a question of efficiency again, we convert the precededBy checkpoint map
        //into a more efficient integer array
        //(that uses checkpoints index defined previously)
        predecessors = new int[checkpoints.size() + 1];
        predecessors[0] = -1;
        for (int i = 1; i <= checkpoints.size(); ++i) {
            Checkpoint before = precededBy.get(checkpoints.get(i - 1));
            if (before == null) {
                predecessors[i] = -1;
            } else {
                predecessors[i] = checkpoints.indexOf(before) + 1;
            }
        }
    }

    /**
     * Executes an adapted "Branch and bound" algorithm to resolve the TSP problem in order
     * to build the most optimized tour
     *
     * @param timeLimit The duration in ms beyond which the TSP algorithm stops
     * @return The best Tour actually found (if the time limit has not been exceeded, it's the best solution possible)
     */
    public Tour searchSolution(int timeLimit) {
        timeLimitReached = false;
        costBestSolution = Double.MAX_VALUE;
        bestSolution = new Integer[nbNodes];

        List<Integer> unseen = new ArrayList<>(nbNodes - 1);
        for (int i = 1; i < nbNodes; i++) {
            unseen.add(i);// We have not visited all the nodes, except 0
        }
        List<Integer> seen = new ArrayList<>(nbNodes);
        seen.add(0); // The first visited node is 0
        //we choose to define additionally this boolean array, in order to verify quickly
        //the constraints of precedence:
        boolean[] seenBoolean = new boolean[nbNodes];

        //We launch the "Branch and bound algorithm"; The initial node is 0 and the initial cost is 0.0
        branchAndBound(0, unseen, seen, seenBoolean, 0.0, cost, System.currentTimeMillis(), timeLimit);

        //Once the algorithm is finished, we must convert the result integer array
        //into a real list of paths, in order to build the result Tour
        List<Path> finalPaths = new ArrayList<>();
        Checkpoint cp;
        Checkpoint lastCp = start;
        for (int indexCp : bestSolution) {
            if (indexCp > 0) {
                cp = checkpoints.get(indexCp - 1);
                finalPaths.add(paths.get(new BasicPair<>(lastCp, cp)));
                lastCp = cp;
            }
        }
        finalPaths.add(paths.get(new BasicPair<>(lastCp, end)));

        return new Tour(finalPaths, tourRequest, tourCompleteGraph);
    }

    /**
     * Indicates if the time limit has been reached or not
     *
     * @return True if the time limit has been reached, false otherwise
     */
    public boolean getTimeLimitReached() {
        return timeLimitReached;
    }

    /**
     * Return the total cost of the best solution found
     * (Be careful ! this total cost does not take in account the waiting time
     * at each checkpoint)
     *
     * @return the total cost of the best solution (!without waiting time!)
     */
    public double getCostBestSolution() {
        return costBestSolution;
    }

    /**
     * Gives a lower bound of the distance to travel to see all unseen checkpoints
     *
     * @param currentNode The node where we are currently in research
     * @param unseen      The unseen checkpoints
     * @param cost        A 2d array of the cost to travel between each couple of checkpoints
     * @return the lower bound mentioned above
     */
    protected abstract double bound(Integer currentNode, List<Integer> unseen, double[][] cost);

    /**
     * Return an iterator to browse efficiently all unseen checkpoints
     *
     * @param currentNode  The node where we are currently in research
     * @param unseen       The unseen checkpoints
     * @param seenBoolean  The already seen checkpoints (in the form of a boolean array, no information of order)
     * @param cost         A 2d array of the cost to travel between each couple of checkpoints
     * @param predecessors An array that indicates if a checkpoint must be preceded by an other checkpoint
     * @return An iterator to travel as well as possible to all the checkpoints not yet seen
     */
    protected abstract Iterator<Integer> iterator(Integer currentNode, List<Integer> unseen, boolean[] seenBoolean, double[][] cost, int[] predecessors);

    /**
     * The heart of the method "Branch and bound" to resolve the travelling salesman problem
     *
     * @param currentNode   The node where we are currently in research
     * @param unseen        The unseen checkpoints
     * @param seen          The already seen checkpoints
     * @param seenBoolean   The already seen checkpoints (in the form of a boolean array, no information of order)
     * @param costSeen      The total cost to travel threw all the already seen checkpoints
     * @param cost          A 2d array of the cost to travel between each couple of checkpoints
     * @param beginningTime System time when we begin to execute the algorithm
     * @param timeLimit     Maximum execution time (in ms) before the algorithm stops
     */
    void branchAndBound(int currentNode, List<Integer> unseen, List<Integer> seen, boolean[] seenBoolean, double costSeen, double[][] cost, long beginningTime, int timeLimit) {
        if (System.currentTimeMillis() - beginningTime > timeLimit) {
            //If the time limit has been reached, we stop the algorithm
            timeLimitReached = true;
            return;
        }
        if (unseen.isEmpty()) { // All the checkpoints have been visited
            costSeen += cost[currentNode][0];//cost to go from last node to the warehouse
            if (costSeen < costBestSolution) { // better solution found
                seen.toArray(bestSolution);
                costBestSolution = costSeen;
            }
        } else if (costSeen + bound(currentNode, unseen, cost) < costBestSolution) {
            //if we enter here, that means this branch can potentially give us a better solution
            Iterator<Integer> it = iterator(currentNode, unseen, seenBoolean, cost, predecessors);
            while (it.hasNext()) {//We try to add all unseen checkpoints to the current path
                Integer nextNode = it.next();
                seen.add(nextNode);
                seenBoolean[nextNode] = true;
                unseen.remove(nextNode);
                branchAndBound(nextNode, unseen, seen, seenBoolean, costSeen + cost[currentNode][nextNode], cost, beginningTime, timeLimit);
                seen.remove(nextNode);
                seenBoolean[nextNode] = false;
                unseen.add(nextNode);
            }
        }
    }
}


