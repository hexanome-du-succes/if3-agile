package com.insa.h4112.model.algorithm;

/**
 * Simple class to represent a Pair of any given Object
 * First element must be Comparable
 *
 * @author Jacques CHARNAY
 */
public class SortPair<A extends Comparable<A>, B> extends BasicPair<A, B> implements Comparable<SortPair<A, B>> {

    /**
     * Constructor
     *
     * @param first  first element
     * @param second second element
     */
    public SortPair(A first, B second) {
        super(first, second);
    }

    @Override
    public int compareTo(SortPair<A, B> abSortPair) {
        return first.compareTo(abSortPair.first);
    }
}
