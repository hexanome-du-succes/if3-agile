package com.insa.h4112.model.algorithm;

import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.Section;

import java.util.*;

/**
 * Algorithm class to execute a Dijkstra on a map
 * and retrieve easily the results
 *
 * @author Jacques CHARNAY
 */
public class DijkstraDistanceMap {

    /**
     * Map used for the Dijkstra algorithm
     */
    private com.insa.h4112.model.domain.Map map;

    /**
     * Starting intersection of the algorithm
     */
    private Intersection origin;

    /**
     * Cost map of the algorithm
     */
    private java.util.Map<Intersection, Double> distances;

    /**
     * Predecessor intersection map of the algorithm
     */
    private java.util.Map<Intersection, Intersection> predecessorsIntersection;

    /**
     * Predecessor section map of the algorithm
     */
    private java.util.Map<Intersection, Section> predecessorsSection;

    /**
     * Constructor
     *
     * @param map    map to use
     * @param origin origin of the Dijkstra
     * @param goals  intersection to reach
     */
    public DijkstraDistanceMap(com.insa.h4112.model.domain.Map map, Intersection origin, Set<Intersection> goals) {
        this.map = map;
        this.origin = origin;
        distances = new HashMap<>();
        predecessorsIntersection = new HashMap<>();
        predecessorsSection = new HashMap<>();

        calculateDistancesDijkstra(goals);
    }

    /**
     * return the origin
     *
     * @return origin
     */
    public Intersection getOrigin() {
        return origin;
    }

    /**
     * return map of intersection and double that representing cost to reach it
     *
     * @return map of intersection => cost
     */
    public java.util.Map<Intersection, Double> getDistances() {
        return distances;
    }

    /**
     * return map for each intersection the predecessor one
     *
     * @return map with intersection and predecessor intersection
     */
    public java.util.Map<Intersection, Intersection> getPredecessorsIntersection() {
        return predecessorsIntersection;
    }

    /**
     * return map of intersection and the section of origin
     *
     * @return map of intersection and his origin section
     */
    public Map<Intersection, Section> getPredecessorsSection() {
        return predecessorsSection;
    }

    /**
     * @param inter the intersection for which you want the distance from origin
     * @return minimal distance between the origin and the intersection
     */
    public double distanceToInter(Intersection inter) {
        return distances.get(inter);
    }

    /**
     * Build the path from origin to the given intersection
     *
     * @param destination the end intersection of the path
     * @return the path mentioned above
     */
    public List<Section> getPathTo(Intersection destination) {
        ArrayList<Section> path = new ArrayList<>();
        if (predecessorsIntersection.get(destination) != null) {
            buildPath(path, destination);
        }
        return path;
    }

    /**
     * Build recursively the path to an intersection
     *
     * @param path         the list containing the path
     * @param currentInter the current intersection in the path
     */
    private void buildPath(ArrayList<Section> path, Intersection currentInter) {
        if (currentInter != origin) {
            buildPath(path, predecessorsIntersection.get(currentInter));
            path.add(predecessorsSection.get(currentInter));
        }
    }

    /**
     * Apply Dijkstra algorithm to calculate the minimal distance from origin to all intersections
     * on the map.
     * Also keep the precedessor of all intersections inside the best path
     *
     * @param checkpointGoals we stop the algorithm once we find all intersections inside checkpointGoals
     */
    private void calculateDistancesDijkstra(Set<Intersection> checkpointGoals) {
        //At the beginning, every intersection is at an infinite distance
        //from the origin
        for (Intersection inter : map.getAllIntersectionsAsList()) {
            distances.put(inter, Double.MAX_VALUE);
            predecessorsIntersection.put(inter, null);
            predecessorsSection.put(inter, null);
        }
        //Only the origin is at a null distance:
        distances.put(origin, 0.0);

        PriorityQueue<SortPair<Double, Intersection>> priority = new PriorityQueue<>();
        priority.add(new SortPair(0.0, origin));

        SortPair<Double, Intersection> currentNode;
        double currentDistance, newDistance;
        Intersection currentInter, neighbour;
        int reachedGoals = 0;
        while (!priority.isEmpty() && (reachedGoals < checkpointGoals.size() || checkpointGoals.isEmpty())) {
            currentNode = priority.poll();
            currentDistance = currentNode.getFirst();
            currentInter = currentNode.getSecond();

            if (currentDistance == distances.get(currentInter)) {
                for (Section neighbourSection : currentInter.getOutgoingSections()) {
                    neighbour = neighbourSection.getDestination();
                    newDistance = currentDistance + neighbourSection.getLength();

                    if (newDistance < distances.get(neighbour)) {
                        distances.put(neighbour, newDistance);
                        predecessorsIntersection.put(neighbour, currentInter);
                        predecessorsSection.put(neighbour, neighbourSection);
                        priority.add(new SortPair(newDistance, neighbour));
                    }
                }

                //We count the numbers of reached goals
                //to stop when we find them all
                if (checkpointGoals.contains(currentInter)) {
                    reachedGoals++;
                }
            }
        }
    }
}
