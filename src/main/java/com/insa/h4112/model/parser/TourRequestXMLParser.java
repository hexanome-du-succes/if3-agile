package com.insa.h4112.model.parser;

import com.insa.h4112.model.domain.*;
import com.insa.h4112.util.ResourceLocator;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaderJDOMFactory;
import org.jdom2.input.sax.XMLReaderXSDFactory;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

/**
 * This class parses the XML data to create the corresponding business objects used by the tour request
 *
 * @author Paul Goux
 */
public class TourRequestXMLParser {

    private static final String XSL_PATH = ResourceLocator.locateResource("TourRequestSchema.xsd");

    /**
     * Parses the xml file to create the tour request
     *
     * @param xmlFile the tour request XML
     * @param map     the map of the application
     * @return TourRequest the tour request with all the deliveries of the request
     */
    public static TourRequest parseTourRequest(File xmlFile, Map map) throws JDOMException, IOException {
        XMLReaderJDOMFactory factory = new XMLReaderXSDFactory(XSL_PATH);
        SAXBuilder saxBuilder = new SAXBuilder(factory);
        TourRequest tourRequest = new TourRequest();

        Document document = saxBuilder.build(xmlFile);
        Element racine = document.getRootElement();

        Element warehouseElement = racine.getChild("entrepot");
        Intersection warehouseIntersection = map.getIntersection(warehouseElement.getAttribute("adresse").getLongValue());
        LocalTime startTime;
        try {
            startTime = LocalTime.parse(warehouseElement.getAttribute("heureDepart").getValue(), DateTimeFormatter.ofPattern("H:m:s"));
        } catch (DateTimeParseException dtpe) {
            throw new JDOMParseException(dtpe.getMessage(), dtpe);
        }
        Checkpoint warehouseStartCheckpoint = new Checkpoint(warehouseIntersection);
        Checkpoint warehouseEndCheckpoint = new Checkpoint(warehouseIntersection);
        Warehouse warehouse = new Warehouse(startTime, warehouseStartCheckpoint, warehouseEndCheckpoint);
        tourRequest.setWarehouse(warehouse);

        List<Element> deliveryElements = racine.getChildren("livraison");
        for (Element deliveryElement : deliveryElements) {
            long pickUpIntersectionId = deliveryElement.getAttribute("adresseEnlevement").getLongValue();
            Intersection pickUpIntersection = map.getIntersection(pickUpIntersectionId);
            if (pickUpIntersection == null) {
                throw new JDOMException("pickUpIntersection id " + pickUpIntersectionId + " not found in given map");
            }

            long deliveryIntersectionId = deliveryElement.getAttribute("adresseLivraison").getLongValue();
            Intersection deliveryIntersection = map.getIntersection(deliveryIntersectionId);
            if (deliveryIntersection == null) {
                throw new JDOMException("deliveryIntersection id " + deliveryIntersectionId + " not found in given map");
            }

            Duration pickUpDuration = durationFromString(deliveryElement.getAttribute("dureeEnlevement").getValue());
            Duration deliveryDuration = durationFromString(deliveryElement.getAttribute("dureeLivraison").getValue());
            Checkpoint pickUpCheckpoint = new Checkpoint(pickUpDuration, pickUpIntersection);
            Checkpoint deliveryCheckpoint = new Checkpoint(deliveryDuration, deliveryIntersection);
            tourRequest.addDelivery(new Delivery(pickUpCheckpoint, deliveryCheckpoint));
        }

        return tourRequest;
    }

    private static Duration durationFromString(String durationString) {
        return Duration.ofSeconds(Integer.parseInt(durationString));
    }
}
