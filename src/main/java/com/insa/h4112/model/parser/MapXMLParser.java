package com.insa.h4112.model.parser;

import com.insa.h4112.model.domain.Intersection;
import com.insa.h4112.model.domain.Map;
import com.insa.h4112.model.domain.Section;
import com.insa.h4112.util.ResourceLocator;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaderJDOMFactory;
import org.jdom2.input.sax.XMLReaderXSDFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * This class parses the XML data to create the corresponding business objects used by the map
 *
 * @author Paul Goux
 */
public class MapXMLParser {

    private static final String XSL_PATH = ResourceLocator.locateResource("MapSchema.xsd");

    /**
     * Parse a XML file to create the map
     *
     * @param xmlFile file containing the data
     * @return Map the map
     */
    public static Map parseMap(File xmlFile) throws JDOMException, IOException {
        XMLReaderJDOMFactory factory = new XMLReaderXSDFactory(XSL_PATH);
        SAXBuilder saxBuilder = new SAXBuilder(factory);
        Map map = new Map();

        Document document = saxBuilder.build(xmlFile);
        Element racine = document.getRootElement();

        List<Element> intersectionElements = racine.getChildren("noeud");
        for (Element intersectionElement : intersectionElements) {
            long id = intersectionElement.getAttribute("id").getLongValue();
            double latitude = intersectionElement.getAttribute("latitude").getDoubleValue();
            double longitude = intersectionElement.getAttribute("longitude").getDoubleValue();
            map.addIntersection(new Intersection(id, latitude, longitude));
        }

        List<Element> sectionElements = racine.getChildren("troncon");
        for (Element sectionElement : sectionElements) {
            long destination = sectionElement.getAttribute("destination").getLongValue();
            double length = sectionElement.getAttribute("longueur").getDoubleValue();
            String streetName = sectionElement.getAttribute("nomRue").getValue();
            long origin = sectionElement.getAttribute("origine").getLongValue();

            Intersection destinationIntersection = map.getIntersection(destination);
            Intersection originIntersection = map.getIntersection(origin);

            Section section = new Section(originIntersection, destinationIntersection, streetName, length);

            originIntersection.addOutgoingSection(section);
        }

        return map;
    }

}
