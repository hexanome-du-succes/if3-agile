package com.insa.h4112.model.parser;

import com.insa.h4112.model.domain.*;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.laughingpanda.beaninject.Inject;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

/**
 * @author Paul Goux & Martin Franceschi
 */
public class TourRequestXMLParserTest {

    private static final String DIR_PATH = "src" + File.separator + "test" + File.separator + "ressources" + File.separator + "parser" + File.separator + "delivery" + File.separator;
    private static Map mapMinimal;
    private static Map mapBig;
    private static Checkpoint fakeCheckpoint = new Checkpoint(new Intersection(0L, 0.0, 0.0));

    @BeforeClass
    public static void setup() throws JDOMException, IOException {
        mapMinimal = MapXMLParser.parseMap(new File(MapXMLParserTest.DIR_PATH + "Map_Minimal.xml"));
        mapBig = MapXMLParser.parseMap(new File(MapXMLParserTest.DIR_PATH + "Map_Big.xml"));
    }

    @Before
    public void beforeEachTest() {
        //Trick used to reset the private static field:
        Inject.field("nextId").of(fakeCheckpoint).with(0);
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidTourRequest_InvalidAddress_WrongType() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "InvalidTourRequest_InvalidAddress_WrongType.xml"), mapMinimal);
    }

    @Test(expected = JDOMException.class)
    public void InvalidTourRequest_InvalidAddress_DeliveryOutOfRange() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "InvalidTourRequest_InvalidAddress_DeliveryOutOfRange.xml"), mapMinimal);
    }

    @Test(expected = JDOMException.class)
    public void InvalidTourRequest_InvalidAddress_PickupOutOfRange() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "InvalidTourRequest_InvalidAddress_PickupOutOfRange.xml"), mapMinimal);
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidTourRequest_InvalidDuration() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "InvalidTourRequest_InvalidDuration.xml"), mapMinimal);
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidTourRequest_InvalidHour1Empty() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "InvalidTourRequest_InvalidHour1Empty.xml"), mapMinimal);
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidTourRequest_InvalidHour2Incorrect() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "InvalidTourRequest_InvalidHour2Incorrect.xml"), mapMinimal);
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidTourRequest_NoDelivery() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "InvalidTourRequest_NoDelivery.xml"), mapMinimal);
    }

    @Test(expected = JDOMParseException.class)
    public void InvalidTourRequest_NoWarehouse() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "InvalidTourRequest_NoWarehouse.xml"), mapMinimal);
    }

    @Test
    public void ValidTourRequest_DuplicateDelivery() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_DuplicateDelivery.xml"), mapMinimal);
    }

    @Test
    public void ValidTourRequest_EmptyDuration() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_EmptyDuration.xml"), mapMinimal);
    }

    @Test
    public void ValidTourRequest_Minimal() throws JDOMException, IOException {
        TourRequest tourRequest = TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_Minimal.xml"), mapMinimal);

        //Trick used to reset the private static field:
        Inject.field("nextId").of(fakeCheckpoint).with(0);
        TourRequest expectedTourRequest = new TourRequest();

        Checkpoint warehouseBeginCheckpoint = new Checkpoint(mapMinimal.getIntersection(1));
        Checkpoint warehouseEndCheckpoint = new Checkpoint(mapMinimal.getIntersection(1));
        Warehouse warehouse = new Warehouse(LocalTime.parse("00:00:00"), warehouseBeginCheckpoint, warehouseEndCheckpoint);
        expectedTourRequest.setWarehouse(warehouse);

        Checkpoint pickupCheckpoint = new Checkpoint(Duration.ofSeconds(1), mapMinimal.getIntersection(2));
        Checkpoint deliveryCheckpoint = new Checkpoint(Duration.ofSeconds(4), mapMinimal.getIntersection(3));
        Delivery delivery = new Delivery(pickupCheckpoint, deliveryCheckpoint);

        expectedTourRequest.addDelivery(delivery);

        assertEquals(expectedTourRequest, tourRequest);
    }

    @Test
    public void ValidTourRequest_ProvidedBig7() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_ProvidedBig7.xml"), mapBig);
    }

    @Test
    public void ValidTourRequest_ProvidedBig9() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_ProvidedBig9.xml"), mapBig);
    }

    @Test
    public void ValidTourRequest_ProvidedMedium3() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_ProvidedMedium3.xml"), mapBig);
    }

    @Test
    public void ValidTourRequest_ProvidedMedium5() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_ProvidedMedium5.xml"), mapBig);
    }

    @Test
    public void ValidTourRequest_ProvidedSmall1() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_ProvidedSmall1.xml"), mapBig);
    }

    @Test
    public void ValidTourRequest_ProvidedSmall2() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_ProvidedSmall2.xml"), mapBig);
    }

    @Test
    public void ValidTourRequest_SameAddress() throws JDOMException, IOException {
        TourRequestXMLParser.parseTourRequest(new File(DIR_PATH + "ValidTourRequest_SameAddress.xml"), mapMinimal);
    }
}