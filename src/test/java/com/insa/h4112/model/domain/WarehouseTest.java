package com.insa.h4112.model.domain;

import org.junit.Before;
import org.junit.Test;
import org.laughingpanda.beaninject.Inject;

import java.time.Duration;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

public class WarehouseTest {
    private static Checkpoint fakeCheckpoint = new Checkpoint(new Intersection(0L, 0.0, 0.0));
    private static Checkpoint start1;
    private static Checkpoint end1;
    private static Checkpoint start2;
    private static Checkpoint end2;
    private static Warehouse warehouse1;
    private static Warehouse warehouse2;

    @Before
    public void beforeTests() {
        Inject.field("nextId").of(fakeCheckpoint).with(0);
        start1 = new Checkpoint(LocalTime.MIDNIGHT, Duration.ofSeconds(3), new Intersection(1l, 0.0, 0.0));
        end1 = new Checkpoint(LocalTime.MIDNIGHT, Duration.ofSeconds(4), new Intersection(2l, 0.0, 0.0));
        start2 = new Checkpoint(LocalTime.MIDNIGHT, Duration.ofSeconds(5), new Intersection(3l, 0.0, 0.0));
        end2 = new Checkpoint(LocalTime.MIDNIGHT, Duration.ofSeconds(6), new Intersection(4l, 0.0, 0.0));
        warehouse1 = new Warehouse(LocalTime.MIN, start1, end1);
        warehouse2 = new Warehouse(LocalTime.MIN, start1, end1);
    }

    @Test
    public void setStart() {
        warehouse1.setStart(start2);
        assertEquals(start2, warehouse1.getStart());
    }

    @Test
    public void setEnd() {
        warehouse1.setEnd(end2);
        assertEquals(end2, warehouse1.getEnd());
    }

    @Test
    public void getSeed() {
        assertEquals(0, warehouse2.getSeed());
        warehouse2.setStart(start2);
        warehouse2.setEnd(end2);
        assertEquals(2, warehouse2.getSeed());
    }
}