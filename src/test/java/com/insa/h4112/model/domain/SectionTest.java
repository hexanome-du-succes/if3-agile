package com.insa.h4112.model.domain;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SectionTest {
    private static Intersection intersection1;
    private static Intersection intersection2;
    private static Intersection intersection3;
    private static Section section1;
    private static Section section2;
    private static Section section3;

    private static Intersection interDown = new Intersection(4l, 10.0 - 1.0, 10.0 + 0.0);
    private static Intersection interUp = new Intersection(5l, 10.0 + 1.0, 10.0 + 0.0);
    private static Intersection interLeft = new Intersection(6l, 10.0 + 0.0, 10.0 - 1.0);
    private static Intersection interRight = new Intersection(7l, 10.0 + 0.0, 10.0 + 1.0);
    private static Intersection interDownLeft = new Intersection(8l, 10.0 - 1.0, 10.0 - 1.0);
    private static Intersection interDownRight = new Intersection(9l, 10.0 - 1.0, 10.0 + 1.0);
    private static Intersection interUpLeft = new Intersection(10l, 10.0 + 1.0, 10.0 - 1.0);
    private static Intersection interUpRight = new Intersection(11l, 10.0 + 1.0, 10.0 + 1.0);
    private static Intersection interCenter = new Intersection(12l, 10.0 + 0.0, 10.0 + 0.0);

    private static Section down_center = new Section(interDown, interCenter, "", 1.0);
    private static Section up_center = new Section(interUp, interCenter, "", 1.0);
    private static Section left_center = new Section(interLeft, interCenter, "", 1.0);
    private static Section right_center = new Section(interRight, interCenter, "", 1.0);
    private static Section downLeft_center = new Section(interDownLeft, interCenter, "", 1.0);
    private static Section upLeft_center = new Section(interUpLeft, interCenter, "", 1.0);
    private static Section downRight_center = new Section(interDownRight, interCenter, "", 1.0);
    private static Section upRight_center = new Section(interUpRight, interCenter, "", 1.0);

    private static Section center_down = new Section(interCenter, interDown, "", 1.0);
    private static Section center_up = new Section(interCenter, interUp, "", 1.0);
    private static Section center_left = new Section(interCenter, interLeft, "", 1.0);
    private static Section center_right = new Section(interCenter, interRight, "", 1.0);
    private static Section center_downLeft = new Section(interCenter, interDownLeft, "", 1.0);
    private static Section center_upLeft = new Section(interCenter, interUpLeft, "", 1.0);
    private static Section center_downRight = new Section(interCenter, interDownRight, "", 1.0);
    private static Section center_upRight = new Section(interCenter, interUpRight, "", 1.0);

    @BeforeClass
    public static void beforeTests() {
        intersection1 = new Intersection(1l, 0.0, 0.0);
        intersection2 = new Intersection(2l, 3.0, 3.0);
        intersection3 = new Intersection(3l, 7.0, 1.0);
        section1 = new Section(intersection1, intersection2, "Street", 54.0);
        section2 = new Section(intersection1, intersection2, "Street", 54.0);
        section3 = new Section(intersection1, intersection2, "Street", 54.0);
    }

    @Test
    public void setOrigin() {
        section1.setOrigin(intersection3);
        assertEquals(intersection3, section1.getOrigin());
    }

    @Test
    public void setDestination() {
        section1.setDestination(intersection3);
        assertEquals(intersection3, section1.getDestination());
    }

    @Test
    public void setStreetName() {
        section1.setStreetName("Rue Paul Jambonx");
        assertEquals("Rue Paul Jambonx", section1.getStreetName());
    }

    @Test
    public void setLength() {
        section1.setLength(3.14);
        assertEquals(3.14, section1.getLength(), 0.0);
    }

    @Test
    public void getPreciseDirectionToNextSection() {
        PreciseDirection dir;

        List<Section> outgoingCenter = new ArrayList<Section>();
        outgoingCenter.add(center_down);
        outgoingCenter.add(center_downLeft);
        outgoingCenter.add(center_downRight);
        outgoingCenter.add(center_left);
        outgoingCenter.add(center_right);
        outgoingCenter.add(center_up);
        outgoingCenter.add(center_upLeft);
        outgoingCenter.add(center_upRight);
        interCenter.setOutgoingSections(outgoingCenter);

        //Starting from Down
        dir = down_center.getPreciseDirectionToNextSection(center_downLeft);
        assertEquals(Direction.LEFT, dir.getDirection());
        assertEquals(1, dir.getExitNumber());
        dir = down_center.getPreciseDirectionToNextSection(center_left);
        assertEquals(Direction.LEFT, dir.getDirection());
        assertEquals(2, dir.getExitNumber());
        dir = down_center.getPreciseDirectionToNextSection(center_upLeft);
        assertEquals(Direction.LEFT, dir.getDirection());
        assertEquals(3, dir.getExitNumber());
        dir = down_center.getPreciseDirectionToNextSection(center_downRight);
        assertEquals(Direction.RIGHT, dir.getDirection());
        assertEquals(1, dir.getExitNumber());
        dir = down_center.getPreciseDirectionToNextSection(center_right);
        assertEquals(Direction.RIGHT, dir.getDirection());
        assertEquals(2, dir.getExitNumber());
        dir = down_center.getPreciseDirectionToNextSection(center_upRight);
        assertEquals(Direction.RIGHT, dir.getDirection());
        assertEquals(3, dir.getExitNumber());
        dir = down_center.getPreciseDirectionToNextSection(center_up);
        assertEquals(Direction.AHEAD, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());

        outgoingCenter = new ArrayList<>();
        outgoingCenter.add(center_left);
        outgoingCenter.add(center_right);
        outgoingCenter.add(center_up);
        outgoingCenter.add(center_down);
        interCenter.setOutgoingSections(outgoingCenter);

        //Starting from Down
        dir = down_center.getPreciseDirectionToNextSection(center_left);
        assertEquals(Direction.LEFT, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());
        dir = down_center.getPreciseDirectionToNextSection(center_right);
        assertEquals(Direction.RIGHT, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());
        dir = down_center.getPreciseDirectionToNextSection(center_up);
        assertEquals(Direction.AHEAD, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());

        //Starting from Right
        dir = right_center.getPreciseDirectionToNextSection(center_down);
        assertEquals(Direction.LEFT, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());
        dir = right_center.getPreciseDirectionToNextSection(center_up);
        assertEquals(Direction.RIGHT, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());

        //Starting from Left
        dir = left_center.getPreciseDirectionToNextSection(center_down);
        assertEquals(Direction.RIGHT, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());
        dir = left_center.getPreciseDirectionToNextSection(center_up);
        assertEquals(Direction.LEFT, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());

        //Starting from Up
        dir = up_center.getPreciseDirectionToNextSection(center_left);
        assertEquals(Direction.RIGHT, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());
        dir = up_center.getPreciseDirectionToNextSection(center_right);
        assertEquals(Direction.LEFT, dir.getDirection());
        assertEquals(-1, dir.getExitNumber());
    }

    @Test
    public void testEquals() {
        assertTrue(section2.equals(section3));
    }

    @Test
    public void testToString() {
        String expectedString = "Section{originId=1, destinationId=2, streetName='Street', length=54.0}";
        assertEquals(expectedString, section3.toString());
    }
}