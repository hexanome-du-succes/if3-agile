package com.insa.h4112.model.domain;

import org.junit.BeforeClass;
import org.junit.Test;
import org.laughingpanda.beaninject.Inject;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PathTest {
    private static Path path1;
    private static Path path2;
    private static Path path3;
    private static Checkpoint checkpointPickUp;
    private static Checkpoint checkpointDeliver;
    private static Section section1;
    private static List<Section> steps1;
    private static Checkpoint fakeCheckpoint = new Checkpoint(new Intersection(0L, 0.0, 0.0));

    @BeforeClass
    public static void beforeTests() {
        Inject.field("nextId").of(fakeCheckpoint).with(0);
        checkpointPickUp = new Checkpoint(Duration.ofSeconds(3), new Intersection(1l, 0.0, 0.0));
        checkpointDeliver = new Checkpoint(Duration.ofSeconds(5), new Intersection(2l, 3.0, 3.0));
        section1 = new Section(checkpointPickUp.getIntersection(), checkpointDeliver.getIntersection(), "Street", 17.0);
        steps1 = new ArrayList<>();
        steps1.add(section1);
        path1 = new Path(checkpointPickUp, checkpointDeliver, steps1);
        path2 = new Path(checkpointPickUp, checkpointDeliver, steps1);
        path3 = new Path(checkpointDeliver, checkpointPickUp, new ArrayList<>());
    }

    @Test
    public void getSteps() {
        assertEquals(steps1, path1.getSteps());
    }

    @Test
    public void testHashCode() {
        assertTrue(path1.hashCode() == path2.hashCode());
        assertFalse(path1.hashCode() == path3.hashCode());
        assertFalse(path2.hashCode() == path3.hashCode());
    }

    @Test
    public void testToString() {
        String expectedString = "Path{origin=Checkpoint{id=1, passageTime=null, duration=PT5S, interval=null, intersection=Intersection{id=2, latitude=3.0, longitude=3.0, outgoingSections=[]}}, destination=Checkpoint{id=0, passageTime=null, duration=PT3S, interval=null, intersection=Intersection{id=1, latitude=0.0, longitude=0.0, outgoingSections=[]}}, steps=[]}";
        assertEquals(expectedString, path3.toString());
    }
}