package com.insa.h4112.model.domain;

import com.insa.h4112.model.algorithm.TourCompleteGraph;
import org.junit.BeforeClass;
import org.junit.Test;
import org.laughingpanda.beaninject.Inject;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class TourTest {
    private static Intersection interDown = new Intersection(4l, 10.0 - 1.0, 10.0 + 0.0);
    private static Intersection interUp = new Intersection(5l, 10.0 + 1.0, 10.0 + 0.0);
    private static Intersection interLeft = new Intersection(6l, 10.0 + 0.0, 10.0 - 1.0);
    private static Intersection interRight = new Intersection(7l, 10.0 + 0.0, 10.0 + 1.0);
    private static Intersection interDownLeft = new Intersection(8l, 10.0 - 1.0, 10.0 - 1.0);
    private static Intersection interDownRight = new Intersection(9l, 10.0 - 1.0, 10.0 + 1.0);
    private static Intersection interUpLeft = new Intersection(10l, 10.0 + 1.0, 10.0 - 1.0);
    private static Intersection interUpRight = new Intersection(11l, 10.0 + 1.0, 10.0 + 1.0);
    private static Intersection interCenter = new Intersection(12l, 10.0 + 0.0, 10.0 + 0.0);
    private static Intersection interOutside = new Intersection(13l, 77.0, 77.0);

    private static Section down_center = new Section(interDown, interCenter, "", 1.0);
    private static Section up_center = new Section(interUp, interCenter, "", 1.0);
    private static Section left_center = new Section(interLeft, interCenter, "", 1.0);
    private static Section right_center = new Section(interRight, interCenter, "", 1.0);
    private static Section downLeft_center = new Section(interDownLeft, interCenter, "", 1.0);
    private static Section upLeft_center = new Section(interUpLeft, interCenter, "", 1.0);
    private static Section downRight_center = new Section(interDownRight, interCenter, "", 1.0);
    private static Section upRight_center = new Section(interUpRight, interCenter, "", 1.0);

    private static Section center_down = new Section(interCenter, interDown, "", 1.0);
    private static Section center_up = new Section(interCenter, interUp, "", 1.0);
    private static Section center_left = new Section(interCenter, interLeft, "", 1.0);
    private static Section center_right = new Section(interCenter, interRight, "", 1.0);
    private static Section center_downLeft = new Section(interCenter, interDownLeft, "", 1.0);
    private static Section center_upLeft = new Section(interCenter, interUpLeft, "", 1.0);
    private static Section center_downRight = new Section(interCenter, interDownRight, "", 1.0);
    private static Section center_upRight = new Section(interCenter, interUpRight, "", 1.0);

    private static Checkpoint fakeCheckpoint = new Checkpoint(new Intersection(0L, 0.0, 0.0));
    private static Checkpoint pickUp1;
    private static Checkpoint deliver1;
    private static Checkpoint pickUp2;
    private static Checkpoint deliver2;
    private static Checkpoint pickUp3;
    private static Checkpoint deliver3;
    private static Checkpoint pickUp4;
    private static Checkpoint deliver4;
    private static Delivery delivery1;
    private static Delivery delivery2;
    private static Delivery delivery3;
    private static Delivery delivery4;
    private static Checkpoint start1;
    private static Checkpoint end1;
    private static Warehouse warehouse1;
    private static Checkpoint inside1;
    private static Checkpoint outside1;
    private static Checkpoint inside2;
    private static Checkpoint outside2;

    private static Path path0_1;
    private static Path path1_1;
    private static Path path1_2;
    private static Path path2_2;
    private static Path path2_3;
    private static Path path3_3;
    private static Path path3_4;
    private static Path path4_4;
    private static Path path4_0;
    private static List<Path> completePath1;
    private static List<Path> completePathOriginal;

    private static Map map1;
    private static TourRequest tourRequest1;
    private static TourCompleteGraph tourCompleteGraph1;
    private static Tour tour1;

    @BeforeClass
    public static void beforeTests() {
        List<Section> outgoingCenter = new ArrayList<>();
        outgoingCenter.add(center_downLeft);
        outgoingCenter.add(center_down);
        outgoingCenter.add(center_downRight);
        outgoingCenter.add(center_left);
        outgoingCenter.add(center_right);
        outgoingCenter.add(center_up);
        outgoingCenter.add(center_upLeft);
        outgoingCenter.add(center_upRight);
        interCenter.setOutgoingSections(outgoingCenter);

        List<Section> outgoingDown = new ArrayList<>();
        outgoingDown.add(down_center);
        interDown.setOutgoingSections(outgoingDown);
        List<Section> outgoingUp = new ArrayList<>();
        outgoingUp.add(up_center);
        interUp.setOutgoingSections(outgoingUp);
        List<Section> outgoingLeft = new ArrayList<>();
        outgoingLeft.add(left_center);
        interLeft.setOutgoingSections(outgoingLeft);
        List<Section> outgoingRight = new ArrayList<>();
        outgoingRight.add(right_center);
        interRight.setOutgoingSections(outgoingRight);
        List<Section> outgoingDownLeft = new ArrayList<>();
        outgoingDownLeft.add(downLeft_center);
        interDownLeft.setOutgoingSections(outgoingDownLeft);
        List<Section> outgoingDownRight = new ArrayList<>();
        outgoingDownRight.add(downRight_center);
        interDownRight.setOutgoingSections(outgoingDownRight);
        List<Section> outgoingUpLeft = new ArrayList<>();
        outgoingUpLeft.add(upLeft_center);
        interUpLeft.setOutgoingSections(outgoingUpLeft);
        List<Section> outgoingUpRight = new ArrayList<>();
        outgoingUpRight.add(upRight_center);
        interUpRight.setOutgoingSections(outgoingUpRight);

        java.util.Map<Long, Intersection> mapInter = new HashMap<>();
        mapInter.put(interCenter.getId(), interCenter);
        mapInter.put(interLeft.getId(), interLeft);
        mapInter.put(interRight.getId(), interRight);
        mapInter.put(interDown.getId(), interDown);
        mapInter.put(interUp.getId(), interUp);
        mapInter.put(interDownLeft.getId(), interDownLeft);
        mapInter.put(interUpLeft.getId(), interUpLeft);
        mapInter.put(interUpRight.getId(), interUpRight);
        mapInter.put(interDownRight.getId(), interDownRight);
        mapInter.put(interOutside.getId(), interOutside);
        map1 = new Map(mapInter);

        Inject.field("nextId").of(fakeCheckpoint).with(0);
        start1 = new Checkpoint(Duration.ofSeconds(1), interCenter);
        end1 = new Checkpoint(Duration.ofSeconds(1), interCenter);
        pickUp1 = new Checkpoint(Duration.ofSeconds(1), interDown);
        deliver1 = new Checkpoint(Duration.ofSeconds(1), interDownLeft);
        pickUp2 = new Checkpoint(Duration.ofSeconds(1), interLeft);
        deliver2 = new Checkpoint(Duration.ofSeconds(1), interUpLeft);
        pickUp3 = new Checkpoint(Duration.ofSeconds(1), interUp);
        deliver3 = new Checkpoint(Duration.ofSeconds(1), interUpRight);
        pickUp4 = new Checkpoint(Duration.ofSeconds(1), interRight);
        deliver4 = new Checkpoint(Duration.ofSeconds(1), interDownRight);
        inside1 = new Checkpoint(Duration.ofSeconds(4), interCenter);
        outside1 = new Checkpoint(Duration.ofSeconds(4), interOutside);
        inside2 = new Checkpoint(Duration.ofSeconds(4), interUp);
        outside2 = new Checkpoint(Duration.ofSeconds(4), interOutside);

        delivery1 = new Delivery(pickUp1, deliver1);
        delivery2 = new Delivery(pickUp2, deliver2);
        delivery3 = new Delivery(pickUp3, deliver3);
        delivery4 = new Delivery(pickUp4, deliver4);

        List<Delivery> deliveries1 = new ArrayList<>();
        deliveries1.add(delivery1);
        deliveries1.add(delivery2);
        deliveries1.add(delivery3);
        deliveries1.add(delivery4);
        warehouse1 = new Warehouse(LocalTime.MIN, start1, end1);

        List<Section> secList0_1 = new ArrayList<>();
        secList0_1.add(center_down);
        path0_1 = new Path(start1, pickUp1, secList0_1);
        List<Section> secList1_1 = new ArrayList<>();
        secList1_1.add(down_center);
        secList1_1.add(center_downLeft);
        path1_1 = new Path(pickUp1, deliver1, secList1_1);
        List<Section> secList1_2 = new ArrayList<>();
        secList1_2.add(downLeft_center);
        secList1_2.add(center_left);
        path1_2 = new Path(deliver1, pickUp2, secList1_2);
        List<Section> secList2_2 = new ArrayList<>();
        secList2_2.add(left_center);
        secList2_2.add(center_upLeft);
        path2_2 = new Path(pickUp2, deliver2, secList2_2);
        List<Section> secList2_3 = new ArrayList<>();
        secList2_3.add(upLeft_center);
        secList2_3.add(center_up);
        path2_3 = new Path(deliver2, pickUp3, secList2_3);
        List<Section> secList3_3 = new ArrayList<>();
        secList3_3.add(up_center);
        secList3_3.add(center_upRight);
        path3_3 = new Path(pickUp3, deliver3, secList3_3);
        List<Section> secList3_4 = new ArrayList<>();
        secList3_4.add(upRight_center);
        secList3_4.add(center_right);
        path3_4 = new Path(deliver3, pickUp4, secList3_4);
        List<Section> secList4_4 = new ArrayList<>();
        secList4_4.add(right_center);
        secList4_4.add(center_downRight);
        path4_4 = new Path(pickUp4, deliver4, secList4_4);
        List<Section> secList4_0 = new ArrayList<>();
        secList4_0.add(downRight_center);
        path4_0 = new Path(deliver4, end1, secList4_0);

        completePath1 = new ArrayList<>();
        completePath1.add(path0_1);
        completePath1.add(path1_1);
        completePath1.add(path1_2);
        completePath1.add(path2_2);
        completePath1.add(path2_3);
        completePath1.add(path3_3);
        completePath1.add(path3_4);
        completePath1.add(path4_4);
        completePath1.add(path4_0);
        completePathOriginal = new ArrayList<>(completePath1);

        tourRequest1 = new TourRequest(warehouse1, deliveries1);
        tourCompleteGraph1 = new TourCompleteGraph(map1, tourRequest1);
        tour1 = new Tour(completePath1, tourRequest1, tourCompleteGraph1);

    }

    @Test
    public void getMoveCheckpointFromToAllowed() {
        //Inside authorized move
        assertTrue(tour1.getMoveCheckpointFromToAllowed(0, 0));
        assertTrue(tour1.getMoveCheckpointFromToAllowed(7, 7));
        assertTrue(tour1.getMoveCheckpointFromToAllowed(3, 6));
        assertTrue(tour1.getMoveCheckpointFromToAllowed(1, 5));
        assertTrue(tour1.getMoveCheckpointFromToAllowed(7, 8));

        //Outside forbidden move
        assertFalse(tour1.getMoveCheckpointFromToAllowed(8, 7));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(8, 0));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(-20, 2));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(1, 57));

        //Move that doesn't respect precedence
        assertFalse(tour1.getMoveCheckpointFromToAllowed(0, 2));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(1, 0));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(2, 6));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(3, 0));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(4, 6));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(5, 2));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(6, 8));
        assertFalse(tour1.getMoveCheckpointFromToAllowed(7, 6));
    }

    @Test
    public void moveCheckpointFromTo() {
        //Forbidden moves, not executed
        assertFalse(tour1.moveCheckpointFromTo(0, 2));
        assertFalse(tour1.moveCheckpointFromTo(1, 0));
        assertFalse(tour1.moveCheckpointFromTo(2, 6));
        assertFalse(tour1.moveCheckpointFromTo(3, 0));
        assertFalse(tour1.moveCheckpointFromTo(4, 6));
        assertFalse(tour1.moveCheckpointFromTo(5, 2));
        assertFalse(tour1.moveCheckpointFromTo(6, 8));
        assertFalse(tour1.moveCheckpointFromTo(7, 6));

        //basic move
        tour1.moveCheckpointFromTo(1, 8);
        assertEquals(deliver1, tour1.getLastPath().getOrigin());
        //undo move
        tour1.moveCheckpointFromTo(7, 1);
        assertTrue(completePathOriginal.equals(completePath1));

        //basic move
        tour1.moveCheckpointFromTo(4, 0);
        assertEquals(pickUp3, tour1.getFirstPath().getDestination());
        //undo move
        tour1.moveCheckpointFromTo(0, 5);
        assertTrue(completePathOriginal.equals(completePath1));

        //classic move
        tour1.moveCheckpointFromTo(3, 5);
        assertEquals(deliver2, tour1.getCheckpoints().get(4));
        //undo move
        tour1.moveCheckpointFromTo(4, 3);
        assertTrue(completePathOriginal.equals(completePath1));

    }

    @Test
    public void getAddDeliveryAllowed() {
        //2 reachable checkpoints
        Delivery delivery1 = new Delivery(inside1, inside2);
        assertTrue(tour1.getAddDeliveryAllowed(delivery1, 0, 0));
        //1 unreachable checkpoint, the deliver
        Delivery delivery2 = new Delivery(inside1, outside1);
        assertFalse(tour1.getAddDeliveryAllowed(delivery2, 0, 0));
        //1 unreachable checkpoint, the pick-up
        Delivery delivery3 = new Delivery(outside2, inside2);
        assertFalse(tour1.getAddDeliveryAllowed(delivery3, 0, 0));

        //Check forbidden index
        assertTrue(tour1.getAddDeliveryAllowed(delivery1, 8, 8));
        assertTrue(tour1.getAddDeliveryAllowed(delivery1, 4, 6));
        assertFalse(tour1.getAddDeliveryAllowed(delivery1, 4, 1));
    }

    @Test
    public void addAndRemoveDelivery() {
        Delivery delivery1 = new Delivery(inside1, inside2);

        //Add delivery at the beginning of the path
        tour1.addDelivery(delivery1, 0, 0);
        assertEquals(delivery1.getPickUp(), tour1.getPath(0).getDestination());
        assertEquals(delivery1.getDeliver(), tour1.getPath(1).getDestination());
        assertFalse(completePathOriginal.equals(completePath1));
        tour1.removeDeliveryOfCheckpoint(0);
        assertTrue(completePathOriginal.equals(completePath1));

        //Add delivery at the end of the path
        tour1.addDelivery(delivery1, 8, 8);
        assertEquals(delivery1.getPickUp(), tour1.getPath(8).getDestination());
        assertEquals(delivery1.getDeliver(), tour1.getPath(9).getDestination());
        assertEquals(10, tour1.getCheckpoints().size());
        assertFalse(completePathOriginal.equals(completePath1));
        tour1.removeDeliveryOfCheckpoint(9);
        assertEquals(8, tour1.getCheckpoints().size());
        assertTrue(completePathOriginal.equals(completePath1));

        //Add delivery in the middle of the path
        tour1.addDelivery(delivery1, 3, 6);
        assertEquals(delivery1.getPickUp(), tour1.getPath(3).getDestination());
        assertEquals(delivery1.getDeliver(), tour1.getPath(7).getDestination());
        assertFalse(completePathOriginal.equals(completePath1));
        tour1.removeDeliveryOfCheckpoint(7);
        assertTrue(completePathOriginal.equals(completePath1));

        //Forbidden add index, nothing happens
        tour1.addDelivery(delivery1, -7, 24);
        assertTrue(completePathOriginal.equals(completePath1));

        //Forbidden remove index, nothing happens
        tour1.removeDeliveryOfCheckpoint(9);
        assertTrue(completePathOriginal.equals(completePath1));
    }

    @Test
    public void getRemoveDeliveryOfCheckpointAllowed() {
        //Check allowed and forbidden index
        assertTrue(tour1.getRemoveDeliveryOfCheckpointAllowed(0));
        assertTrue(tour1.getRemoveDeliveryOfCheckpointAllowed(7));
        assertFalse(tour1.getRemoveDeliveryOfCheckpointAllowed(-1));
        assertFalse(tour1.getRemoveDeliveryOfCheckpointAllowed(8));
        assertFalse(tour1.getRemoveDeliveryOfCheckpointAllowed(42));
    }

    @Test
    public void getPath() {
        assertEquals(start1, tour1.getPath(0).getOrigin());
    }

    @Test
    public void getFirstPath() {
        assertEquals(start1, tour1.getFirstPath().getOrigin());
    }

    @Test
    public void getLastPath() {
        assertEquals(end1, tour1.getLastPath().getDestination());
    }

    @Test
    public void getAllSectionsInPaths() {
        List<Section> allSections = tour1.getAllSectionsInPaths();
        assertEquals(center_down, allSections.get(0));
        assertEquals(downRight_center, allSections.get(allSections.size() - 1));
    }

    @Test
    public void getAllPaths() {
        assertTrue(completePath1.equals(tour1.getAllPaths()));
    }

    @Test
    public void getAllCheckpoints() {
        List<Checkpoint> allCheckpoints = tour1.getAllCheckpoints();
        assertEquals(start1, allCheckpoints.get(0));
        assertEquals(end1, allCheckpoints.get(allCheckpoints.size() - 1));
    }

    @Test
    public void getTourRequest() {
        assertEquals(tourRequest1, tour1.getTourRequest());
    }

    @Test
    public void getPropertyChangeSupport() {
        assertNotEquals(null, tour1.getPropertyChangeSupport());
    }

    @Test
    public void testEquals() {
        Tour tour2 = new Tour(new ArrayList<Path>(), tourRequest1, tourCompleteGraph1);
        assertFalse(tour1.equals(tour2));
    }

    @Test
    public void testHashCode() {
        Tour tour2 = new Tour(new ArrayList<Path>(), tourRequest1, tourCompleteGraph1);
        assertFalse(tour1.hashCode() == tour2.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("Tour", tour1.toString().substring(0, 4));
    }
}