package com.insa.h4112.model.domain;


import org.junit.Assert;
import org.junit.Test;

import java.time.LocalTime;

public class TimeIntervalTest {

    @Test
    public void contains() {
        TimeInterval it = TimeInterval.computeInterval(LocalTime.of(12, 0));
        Assert.assertTrue(it.contains(LocalTime.of(12, 0)));
        Assert.assertTrue(it.contains(LocalTime.of(14, 0)));
        Assert.assertTrue(it.contains(LocalTime.of(10, 0)));
        Assert.assertTrue(it.contains(LocalTime.of(10, 30)));
        Assert.assertFalse(it.contains(LocalTime.of(14, 30)));
        Assert.assertFalse(it.contains(LocalTime.of(9, 30)));
        Assert.assertFalse(it.contains(LocalTime.of(9, 59)));
        Assert.assertFalse(it.contains(LocalTime.of(14, 1)));
        Assert.assertFalse(it.contains(LocalTime.of(20, 49)));
        Assert.assertFalse(it.contains(LocalTime.of(2, 16)));

        it = TimeInterval.computeInterval(LocalTime.of(1, 0));
        Assert.assertTrue(it.contains(LocalTime.of(1, 0)));
        Assert.assertTrue(it.contains(LocalTime.of(23, 0)));
        Assert.assertTrue(it.contains(LocalTime.of(2, 30)));
        Assert.assertTrue(it.contains(LocalTime.of(3, 0)));
        Assert.assertTrue(it.contains(LocalTime.of(0, 0)));
        Assert.assertTrue(it.contains(LocalTime.of(0, 20)));
        Assert.assertTrue(it.contains(LocalTime.of(23, 58)));
        Assert.assertFalse(it.contains(LocalTime.of(22, 30)));
        Assert.assertFalse(it.contains(LocalTime.of(16, 20)));
        Assert.assertFalse(it.contains(LocalTime.of(12, 0)));
        Assert.assertFalse(it.contains(LocalTime.of(8, 20)));
        Assert.assertFalse(it.contains(LocalTime.of(5, 40)));
        Assert.assertFalse(it.contains(LocalTime.of(3, 30)));
        Assert.assertFalse(it.contains(LocalTime.of(3, 1)));
        Assert.assertFalse(it.contains(LocalTime.of(22, 59)));

        it = TimeInterval.computeInterval(LocalTime.of(23, 30));
        Assert.assertTrue(it.contains(LocalTime.of(1, 30)));
        Assert.assertTrue(it.contains(LocalTime.of(21, 30)));
        Assert.assertTrue(it.contains(LocalTime.of(0, 0)));
        Assert.assertTrue(it.contains(LocalTime.of(0, 18)));
        Assert.assertTrue(it.contains(LocalTime.of(0, 58)));
        Assert.assertTrue(it.contains(LocalTime.of(23, 31)));
        Assert.assertTrue(it.contains(LocalTime.of(1, 0)));
        Assert.assertTrue(it.contains(LocalTime.of(1, 15)));
        Assert.assertFalse(it.contains(LocalTime.of(3, 15)));
        Assert.assertFalse(it.contains(LocalTime.of(6, 15)));
        Assert.assertFalse(it.contains(LocalTime.of(12, 0)));
        Assert.assertFalse(it.contains(LocalTime.of(20, 15)));
        Assert.assertFalse(it.contains(LocalTime.of(16, 48)));
    }

    @Test
    public void computeInterval() {
        LocalTime local = LocalTime.of(12, 0);
        TimeInterval it = TimeInterval.computeInterval(local);
        Assert.assertEquals(it.getStartTime(), LocalTime.of(10, 0));
        Assert.assertEquals(it.getEndTime(), LocalTime.of(14, 0));

        local = LocalTime.of(12, 24);
        it = TimeInterval.computeInterval(local);
        Assert.assertEquals(it.getStartTime(), LocalTime.of(10, 30));
        Assert.assertEquals(it.getEndTime(), LocalTime.of(14, 30));

        local = LocalTime.of(12, 20);
        it = TimeInterval.computeInterval(local);
        Assert.assertEquals(it.getStartTime(), LocalTime.of(10, 15));
        Assert.assertEquals(it.getEndTime(), LocalTime.of(14, 15));

        local = LocalTime.of(23, 2);
        it = TimeInterval.computeInterval(local);
        Assert.assertEquals(it.getStartTime(), LocalTime.of(21, 0));
        Assert.assertEquals(it.getEndTime(), LocalTime.of(1, 0));


        local = LocalTime.of(0, 51);
        it = TimeInterval.computeInterval(local);
        Assert.assertEquals(it.getStartTime(), LocalTime.of(22, 45));
        Assert.assertEquals(it.getEndTime(), LocalTime.of(2, 45));
    }
}
