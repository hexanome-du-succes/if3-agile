package com.insa.h4112.model.algorithm;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

/**
 * @author Hugo REYMOND
 * @author Jacques CHARNAY
 */
public class IteratorOptimizedTest {

    /**
     * Cost map
     */
    private double[][] cost = new double[][]{{
            0, 6, 7, 7, 8 // From Warehouse (0)
    }, {
            6, 0, 10, 3, 5 // From Delivery1 (1)
    }, {
            7, 10, 0, 1, 6 // From Pickup1 (2)
    }, {
            7, 3, 1, 0, 4 // From Delivery2 (3)
    }, {
            8, 5, 6, 4, 0 // From Pickup2 (4)
    }};

    /**
     * Predecessors array.
     */
    private int[] predecessors = new int[]{-1, 4, 0, 2, 0};

    /**
     * Array of seen sections.
     */
    private boolean[] seen = new boolean[]{true, false, false, false, false};

    @Test
    public void IteratorHasNoNext() {
        IteratorOptimized myIterator = new IteratorOptimized(
                0,
                Collections.emptyList(),
                seen,
                cost,
                new int[5]);

        assertFalse(myIterator.hasNext());

    }

    @Test
    public void IteratorNextFromWarehouse() {
        // We are at the warehouse, no checkpoints visited yet.
        IteratorOptimized myIterator = new IteratorOptimized(
                0,
                Arrays.asList(1, 2, 3, 4),
                seen,
                cost,
                predecessors);

        assertTrue(myIterator.hasNext());
        assertEquals(2, myIterator.next().intValue()); // We expect the next point to be Pickup2 (2)
        assertTrue(myIterator.hasNext());
        assertEquals(4, myIterator.next().intValue()); // We expect the next point to be Pickup1 (4)
        assertFalse(myIterator.hasNext());
    }

    @Test
    public void IteratorNextFromPickup() {
        //We move on to Pickup
        seen[2] = true;
        IteratorOptimized myIterator = new IteratorOptimized(
                2,
                Arrays.asList(1, 3, 4),
                seen,
                cost,
                predecessors);

        assertTrue(myIterator.hasNext());
        assertEquals(3, myIterator.next().intValue()); // We expect the next point to be Delivery2 (3)
        assertTrue(myIterator.hasNext());
        assertEquals(4, myIterator.next().intValue()); // We expect the next point to be Pickup1 (4)
        assertFalse(myIterator.hasNext());
    }

    @Test
    public void remove() {
        IteratorOptimized myIterator = new IteratorOptimized(
                0,
                Collections.emptyList(),
                seen,
                cost,
                new int[5]);
        myIterator.remove();
    }
}
