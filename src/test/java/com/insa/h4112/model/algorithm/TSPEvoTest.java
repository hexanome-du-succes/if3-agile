package com.insa.h4112.model.algorithm;

import com.insa.h4112.model.domain.*;
import com.insa.h4112.model.parser.MapXMLParser;
import com.insa.h4112.model.parser.TourRequestXMLParser;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import static org.junit.Assert.*;

/**
 * @author Hugo REYMOND
 * @author Jacques CHARNAY
 */
public class TSPEvoTest {

    public static final String DIR_PATH = "src" + File.separator + "test" + File.separator + "ressources" + File.separator + "parser" + File.separator + "map" + File.separator;
    private static final String DIR_PATH_2 = "src" + File.separator + "test" + File.separator + "ressources" + File.separator + "parser" + File.separator + "delivery" + File.separator;

    private static TourCompleteGraph tourCompleteGraph;
    private static TourRequest tourRequest;
    private static TSPEvo tspEvo;
    private static Map map;

    private static Intersection inter0 = new Intersection(0, 0.0, 0.0);
    private static Intersection inter1 = new Intersection(1, 0.0, 0.0);
    private static Intersection inter2 = new Intersection(2, 0.0, 0.0);
    private static Intersection inter3 = new Intersection(3, 0.0, 0.0);
    private static Intersection inter4 = new Intersection(4, 0.0, 0.0);
    private static Intersection inter5 = new Intersection(5, 0.0, 0.0);

    private static Section se0_2 = new Section(inter0, inter2, "", 3.0);
    private static Section se2_3 = new Section(inter2, inter3, "", 4.0);
    private static Section se0_1 = new Section(inter0, inter1, "", 6.0);
    private static Section se1_0 = new Section(inter1, inter0, "", 6.0);
    private static Section se1_3 = new Section(inter1, inter3, "", 7.0);
    private static Section se3_1 = new Section(inter3, inter1, "", 7.0);
    private static Section se3_4 = new Section(inter3, inter4, "", 10.0);
    private static Section se4_5 = new Section(inter4, inter5, "", 2.0);
    private static Section se5_3 = new Section(inter5, inter3, "", 4.0);

    private static Checkpoint start = new Checkpoint(Duration.ofSeconds(0l), inter0);
    private static Checkpoint end = new Checkpoint(Duration.ofSeconds(0l), inter0);
    private static Checkpoint pickupPoint1 = new Checkpoint(Duration.ofSeconds(0l), inter5);
    private static Checkpoint deliverPoint1 = new Checkpoint(Duration.ofSeconds(0l), inter1);
    private static Checkpoint pickupPoint2 = new Checkpoint(Duration.ofSeconds(0l), inter4);
    private static Checkpoint deliverPoint2 = new Checkpoint(Duration.ofSeconds(0l), inter3);

    @BeforeClass
    public static void beforeTests() {
        inter0.addOutgoingSection(se0_2);
        inter0.addOutgoingSection(se0_1);
        inter1.addOutgoingSection(se1_0);
        inter1.addOutgoingSection(se1_3);
        inter2.addOutgoingSection(se2_3);
        inter3.addOutgoingSection(se3_1);
        inter3.addOutgoingSection(se3_4);
        inter4.addOutgoingSection(se4_5);
        inter5.addOutgoingSection(se5_3);

        HashMap<Long, Intersection> interMap = new HashMap<>();
        interMap.put(0l, inter0);
        interMap.put(1l, inter1);
        interMap.put(2l, inter2);
        interMap.put(3l, inter3);
        interMap.put(4l, inter4);
        interMap.put(5l, inter5);
        map = new Map(interMap);

        Warehouse warehouse = new Warehouse(LocalTime.now(), start, end);
        List<Delivery> deliveries = new ArrayList<>();
        Delivery deliver1 = new Delivery(pickupPoint1, deliverPoint1);
        deliveries.add(deliver1);

        Delivery deliver2 = new Delivery(pickupPoint2, deliverPoint2);
        deliveries.add(deliver2);

        tourRequest = new TourRequest(warehouse, deliveries);
        tourCompleteGraph = new TourCompleteGraph(map, tourRequest);
        tspEvo = new TSPEvo(tourRequest, tourCompleteGraph);
    }

    @Test
    public void testOrder() {
        Tour tour = tspEvo.searchSolution(5000);
        ListIterator<Checkpoint> checkpoints = tour.getCheckpoints().listIterator();
        assertEquals(checkpoints.next(), pickupPoint2);
        assertEquals(checkpoints.next(), pickupPoint1);
        assertEquals(checkpoints.next(), deliverPoint2);
        assertEquals(checkpoints.next(), deliverPoint1);
        assertEquals(checkpoints.hasNext(), false);
        assertEquals(tspEvo.getTimeLimitReached(), false);
    }

    @Test
    public void getCostBestSolution() {
        Tour tour = tspEvo.searchSolution(5000);
        assertEquals(36.0, tspEvo.getCostBestSolution(), 0.0);
    }

    @Test(timeout = 5000)
    public void timeLimitReached() {
        try {
            Map mapBig = MapXMLParser.parseMap(new File(DIR_PATH + "ValidMap_ProvidedBig.xml"));
            TourRequest tourRequestBig = TourRequestXMLParser.parseTourRequest(new File(DIR_PATH_2 + "ValidTourRequest_ProvidedBig9.xml"), mapBig);
            TourCompleteGraph tourCompleteGraphBig = new TourCompleteGraph(mapBig, tourRequestBig);
            TemplateTSPEvo tspEvo = new TSPEvo(tourRequestBig, tourCompleteGraphBig);
            tspEvo.searchSolution(200);
            assertTrue(tspEvo.getTimeLimitReached());
        } catch (Exception e) {
            fail();
        }
    }

}
