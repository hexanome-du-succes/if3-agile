package com.insa.h4112.controller.command.util;

import com.insa.h4112.controller.command.Command;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests of StackCommandHistory
 *
 * @author Pierre-Yves GENEST
 */
public class StackCommandHistoryTest {

    private int dos;
    private int undos;
    private StackCommandHistory stackCommandHistory;

    @Test
    public void addCommand() {
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(1, dos);
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(2, dos);
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(3, dos);

        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();

        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(4, dos);
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(5, dos);
    }

    @Test
    public void undoCommand() {
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(3, dos);
        assertEquals(0, undos);

        stackCommandHistory.undoCommand();
        assertEquals(3, dos);
        assertEquals(1, undos);
        stackCommandHistory.undoCommand();
        assertEquals(3, dos);
        assertEquals(2, undos);
        stackCommandHistory.undoCommand();
        assertEquals(3, dos);
        assertEquals(3, undos);
        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        assertEquals(3, dos);
        assertEquals(3, undos);
    }

    @Test
    public void redoCommand() {
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(3, dos);
        assertEquals(0, undos);

        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        assertEquals(3, dos);
        assertEquals(2, undos);

        stackCommandHistory.redoCommand();
        assertEquals(4, dos);
        assertEquals(2, undos);
        stackCommandHistory.redoCommand();
        assertEquals(5, dos);
        assertEquals(2, undos);
        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        assertEquals(5, dos);
        assertEquals(2, undos);

        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        assertEquals(5, dos);
        assertEquals(5, undos);

        stackCommandHistory.redoCommand();
        assertEquals(6, dos);
        assertEquals(5, undos);
        stackCommandHistory.redoCommand();
        assertEquals(7, dos);
        assertEquals(5, undos);
        stackCommandHistory.redoCommand();
        assertEquals(8, dos);
        assertEquals(5, undos);
        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        assertEquals(8, dos);
        assertEquals(5, undos);
    }

    @Test
    public void addSubHistory() {
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(3, dos);
        assertEquals(0, undos);

        stackCommandHistory.addSubHistory();

        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(6, dos);
        assertEquals(0, undos);

        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        assertEquals(6, dos);
        assertEquals(2, undos);
        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        assertEquals(8, dos);
        assertEquals(2, undos);

        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        assertEquals(8, dos);
        assertEquals(5, undos);

        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        assertEquals(8, dos);
        assertEquals(5, undos);

        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        assertEquals(8, dos);
        assertEquals(8, undos);


        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        assertEquals(11, dos);
        assertEquals(8, undos);

    }

    @Test
    public void popSubHistory() {
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(3, dos);
        assertEquals(0, undos);

        stackCommandHistory.addSubHistory();

        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        stackCommandHistory.addCommand(new TestCommand(this));
        assertEquals(6, dos);
        assertEquals(0, undos);

        stackCommandHistory.popSubHistory();

        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        stackCommandHistory.redoCommand();
        assertEquals(6, dos);
        assertEquals(0, undos);

        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        stackCommandHistory.undoCommand();
        assertEquals(6, dos);
        assertEquals(3, undos);
    }

    @Before
    public void before() {
        dos = 0;
        undos = 0;
        stackCommandHistory = new StackCommandHistory();
    }


    public void receiveDo() {
        dos++;
    }

    public void receiveUndo() {
        undos++;
    }

    private class TestCommand implements Command {
        private StackCommandHistoryTest test;

        public TestCommand(StackCommandHistoryTest test) {
            this.test = test;
        }

        @Override
        public void doCommand() {
            test.receiveDo();
        }

        @Override
        public void undoCommand() {
            test.receiveUndo();
        }
    }
}
